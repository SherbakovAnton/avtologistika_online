package com.team.estafeta.avtologistika_online.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.Window
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.R.id.widgetRoot
import com.team.estafeta.avtologistika_online.extension.*
import com.team.estafeta.avtologistika_online.model.db.entity.Company
import com.team.estafeta.avtologistika_online.view.fragment.FragmentListCompany
import com.team.estafeta.avtologistika_online.view.fragment.FragmentLogin
import com.team.estafeta.avtologistika_online.viewModel.LoginViewModel
import com.team.estafeta.avtologistika_online.viewModel.PresenterKpiList
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.WindowFeature

@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
@WindowFeature(Window.FEATURE_NO_TITLE)
open class MainActivity : AppCompatActivity() {

    private val loginViewModel      by lazy { obtainViewModel(LoginViewModel::class) }

    @Bean lateinit var kpiListPresenter : PresenterKpiList

    @AfterViews fun onInit() {

        setFragment(widgetRoot, FragmentLogin.newInstance())

        observe(loginViewModel.companyList) { it?.let { onSuccessLogin(it) } }
        observe(loginViewModel.errorWeb) { it?.let { toast(it.errorRes) } }
    }

    fun onSuccessLogin(companyList : List<Company>?) {
        companyList?.let {
            when {
                it.size == 1 -> {
                    kpiListPresenter.onSelectCompany(companyList.first())

                    startActivity(Intent(this, KpiActivity_::class.java)) + finish()
                }
                it.size > 1 -> replaceFragment(widgetRoot, FragmentListCompany.newInstance())
                else -> toast(string(R.string.no_permissions))
            }
        }
    }



}






