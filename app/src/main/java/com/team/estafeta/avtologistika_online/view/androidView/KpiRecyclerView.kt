package com.team.estafeta.avtologistika_online.view.androidView

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.gson.Gson
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.initialize
import com.team.estafeta.avtologistika_online.extension.measuringSystem
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.AbstractProgressRecyclerView
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.RecyclerViewAdapterBase
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.ViewWrapper
import org.apache.commons.lang3.StringUtils

/**
 * Created by antony on 06.04.18.
 */


class KpiRecyclerView(context: Context, attrs: AttributeSet?) :
        AbstractProgressRecyclerView<Kpi, KpiCellView>(context, attrs) {

    override val adapter        by lazy { KpiAdapter(context).apply { initAdapter() } }

    init { recyclerView.adapter = adapter }

    var onItemClick : ((Kpi) -> Unit)? = null
    set(value) { adapter.onItemClick = value }

}

class KpiCellView(context: Context) : RelativeLayout(context) {

    internal val kpiName by lazy { findViewById<TextView>(R.id.kpiName) }
    internal val kpiValue by lazy { findViewById<TextView>(R.id.kpiValue) }
    internal val measuring by lazy { findViewById<TextView>(R.id.kpiMeasuringSystem) }

    @SuppressLint("SetTextI18n")
    internal fun bindKpi(kpi: Kpi) {
        kpi.run {
            kpiName.text = name
            kpiValue.text = getValue()
            measuring.text = kpi.measuringSystem()
        }
    }

    init { initialize(R.layout.cell_view_kpi) }

}

/** ADAPTER */
class KpiAdapter(private val context: Context) : RecyclerViewAdapterBase<Kpi, KpiCellView>() {

    var onItemClick : ((Kpi) -> Unit)? = null

    override fun onBindViewHolder(holder: ViewWrapper<KpiCellView>?, position: Int) {
        items[position].also { kpi ->
            val view = holder?.view
            view?.bindKpi(kpi)

            view?.setOnClickListener {
                onItemClick?.invoke(kpi)
            }

            view?.setOnLongClickListener {
                val data = ClipData.newPlainText(Gson().toJson(kpi), StringUtils.EMPTY)
                val shadowBuilder = View.DragShadowBuilder(KpiDragAndDropView(context, kpi))
                it.startDrag(data, shadowBuilder, it, 0)
            }
        }
    }

    override fun onCreateItemView(parent: ViewGroup, viewType: Int) = KpiCellView(context)

}




