package com.team.estafeta.avtologistika_online.model.db.realmCore

import io.realm.RealmModel

/**
 * Created by antony on 24.04.18.
 */
sealed class RealmResponse<E : RealmModel>(val entity: E?) {

    companion object {
        fun <E : RealmModel> init(e: E?) : RealmResponse<E> =
                e?.let {
                    return DataBaseResponse(it) }
                        ?: let {
                    return NullEntity() }
    }

    class DataBaseResponse<E : RealmModel>(val e: E) : RealmResponse<E>(e)

    class NullEntity<E : RealmModel> : RealmResponse<E>(null)


    fun entity(query : RealmResponse<E>) : RealmModel? {
        return when (query) {
            is DataBaseResponse<*> -> { query.entity }
            else -> null
        }
    }
}