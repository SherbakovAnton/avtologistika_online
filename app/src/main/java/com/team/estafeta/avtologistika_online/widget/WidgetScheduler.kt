package com.team.estafeta.avtologistika_online.widget

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import com.team.estafeta.avtologistika_online.delegate.appDelegate.log

/**
 * Created by antony on 20.04.18.
 */

object WidgetScheduler {

    private val CODE = this::class.java.simpleName.hashCode()
    private val interval = 1 * 60_000L // 1 min by default

    private var alarmManager : AlarmManager? = null
    private var pendingIntent : PendingIntent? = null

    fun schedule(context: Context) {
        val intent = Intent(context, WidgetIntentService_::class.java)
        pendingIntent = PendingIntent.getService(context, CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmManager = (context.getSystemService(Context.ALARM_SERVICE) as AlarmManager).apply {
            setInexactRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), interval, pendingIntent)
        }
    }

    fun cancelSchedule() {
        pendingIntent?.let {
            log("widget updating is canceled...")
            alarmManager?.cancel(pendingIntent)
            alarmManager = null
            pendingIntent = null
        }
    }
}

