package com.team.estafeta.avtologistika_online.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.team.estafeta.avtologistika_online.model.db.entity.Company
import com.team.estafeta.avtologistika_online.model.db.repository.UserRepository
import com.team.estafeta.avtologistika_online.model.db.repository.saveUser
import com.team.estafeta.avtologistika_online.model.entity.UserTemporary
import com.team.estafeta.avtologistika_online.model.web.ErrorWeb
import com.team.estafeta.avtologistika_online.model.web.Web
import com.team.estafeta.avtologistika_online.model.web.handle
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by antony on 04.04.18.
 */

class LoginViewModel : ViewModel() {

    val companyList     = MutableLiveData<List<Company>>()
    val errorWeb        = MutableLiveData<ErrorWeb>()

    private fun login(user : UserTemporary) {
        Web.getToken(user.login, user.password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { if (it.success != null && !it.success) errorWeb.postValue(ErrorWeb.CredentialsException) }
                .subscribe (    { it.token?.let { token ->
                    user.primaryToken = token
                    onSuccessLogin(user) } },
                                { errorWeb.handle(it) } )
    }

    private fun onSuccessLogin(user : UserTemporary) {
        UserRepository.initFrom(user).saveUser { getCompanyListFromServer() }
    }

    private fun getCompanyListFromServer() {
        Web.getCompanies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                        { serverCompanyList ->
                            UserRepository.getUser {
                                serverCompanyList.forEach { company -> it
                                        ?.companyList?.add(company) }
                                it?.saveUser { companyList.postValue(serverCompanyList) }
                            }
                        },
                        { errorWeb.postValue(ErrorWeb.UnknownError) })
    }

    fun login(login : String, password : String) {
        login(UserTemporary(login, password))
    }
}