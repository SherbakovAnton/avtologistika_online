package com.team.estafeta.avtologistika_online.model.web

import com.team.estafeta.avtologistika_online.R

/**
 * Created by antony on 06.04.18.
 */

enum class ErrorWeb(val errorRes : Int) {

    SocketTimeoutException(R.string.no_internet_connection),
    UnknownHostException(R.string.no_internet_connection),
    CredentialsException(R.string.credentials_not_valid),

    UnknownError(R.string.unknown_error)


}