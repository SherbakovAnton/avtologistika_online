package com.team.estafeta.avtologistika_online.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.databinding.FragmentSettingsBinding
import com.team.estafeta.avtologistika_online.extension.alert
import com.team.estafeta.avtologistika_online.extension.hideGear
import com.team.estafeta.avtologistika_online.extension.setBackArrow
import com.team.estafeta.avtologistika_online.model.entity.UserSettings
import com.team.estafeta.avtologistika_online.viewModel.SettingsViewModel
import kotlinx.android.synthetic.main.fragment_settings.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EFragment
import kotlin.properties.Delegates

/**
 * Created by antony on 12.04.18.
 */

@EFragment(R.layout.fragment_settings)
open class FragmentSettings : Fragment() {

    private var dataBinding         by Delegates.notNull<FragmentSettingsBinding>()
    private val settingsViewModel   by lazy { ViewModelProviders.of(activity!!).get(SettingsViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dataBinding = FragmentSettingsBinding.inflate(inflater, container, false)
        dataBinding.userSettings = UserSettings()

        return dataBinding.root
    }


    @AfterViews fun onInit() {
        setBackArrow()
        hideGear()

//        settingsDark.setOnClickListener {  }
//        settingsWhite.setOnClickListener {  }
//        settingsHorizontally.setOnClickListener {  }
//        settingsVertically.setOnClickListener {  }
        settingsCompany.setOnClickListener {  }
        settingsLogout.setOnClickListener { onLogOut() }
    }

    private fun onLogOut() {
        alert {
            message = "Are you shure you want to LogOut?"
            onResult = { settingsViewModel.logout(activity!!) }
        }
    }


    companion object { fun newInstance() : FragmentSettings = FragmentSettings_.builder().build() }
}