package com.team.estafeta.avtologistika_online.model.db.entity

import android.util.Base64
import com.team.estafeta.avtologistika_online.model.db.realmCore.DataEntity
import io.realm.RealmList
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by antony on 02.04.18.
 */
@RealmClass open class User : DataEntity {

    @PrimaryKey var id = 1

    var token       : String? = null
    var companyId   : String? = null
    var login       : String? = null
    var password    : String? = null
    var companyList : RealmList<Company>? = RealmList()
    var settings : UserSettings? = UserSettings()

    private var state = UserState.logOut.ordinal

    fun auth() : String {
        var encodedToken = token
        if (companyId != null) encodedToken = "$encodedToken@$companyId"
        return "Token ${Base64.encodeToString(encodedToken?.toByteArray(), Base64.NO_WRAP)}"
    }

    fun setState(state : UserState) {
        this.state = state.ordinal
    }

    fun getState() : UserState {
        return UserState.values().find { it.ordinal == state } ?: UserState.logOut
    }

    override fun toString() = asString
}

enum class UserState { logOut, logIn, companySelected }
