package com.team.estafeta.avtologistika_online.extension

import android.arch.lifecycle.*
import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.view.activity.KpiActivity
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_kpi.*
import kotlin.reflect.KClass

/**
 * Created by antony on 03.04.18.
 */


fun <T : ViewModel> Fragment.obtainViewModel(viewModelClass: KClass<T>) =
        ViewModelProviders.of(this).get(viewModelClass.java)

fun <T : ViewModel> Fragment.obtainViewModel(viewModelClass: KClass<T>, initFoo : () -> T) =
        ViewModelProviders.of(this).get(viewModelClass.java).apply { initFoo() }

fun <T> Fragment.observe(liveData: LiveData<T>, callback: (T?) -> Unit) {
    liveData.observe(this, Observer { callback.invoke(it) })
}

fun Fragment.lifeCycle(observer : LifecycleObserver) = this.lifecycle.addObserver(observer)

/*******************     TOOLBAR  ****************/

fun Fragment.setBackArrow() = this.handleBackArrow(true)

fun Fragment.disableBackArrow() = this.handleBackArrow(false)

private fun Fragment.handleBackArrow(isSet : Boolean) {
    (activity as AppCompatActivity).supportActionBar?.run {
        setDisplayHomeAsUpEnabled(isSet)
        setDisplayShowHomeEnabled(isSet)
    }
}

fun Fragment.showGear() = this.handleSettingsGear(true)
fun Fragment.hideGear() = this.handleSettingsGear(false)

private fun Fragment.handleSettingsGear(isSet : Boolean) {
    (activity as? KpiActivity)?.toolbarSettings?.run {
        visibility = if(isSet) View.VISIBLE else View.INVISIBLE
    }
}

fun Fragment.addFragment(root : Int, fragment: Fragment) {
    activity!!
            .supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(root, fragment)
            .addToBackStack(null)
            .commit()
}


fun Fragment.setFragment(root : Int, fragment: Fragment) = this.activity?.supportFragmentManager
        ?.beginTransaction()
        ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
        ?.add(root, fragment)
        ?.addToBackStack(null)
        ?.commit()

fun Fragment.swipeFragmentToUp() {
    this.activity?.supportFragmentManager?.beginTransaction()?.apply {
        //setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out)
        remove(this@swipeFragmentToUp)
    }?.commit()
}

/***************************************** ALERTS *************************************************/

data class AlertBuilder(
        var title: String? = null,
        var message: Any? = null,
        var okRes: Int? = R.string.ok,
        var cancelRes: Int? = null /*R.string.cancel*/,
        var onResult: (() -> Unit)? = null,
        var onCancel: (() -> Unit)? = null,
        var viewList: List<View>? = null
)

/**
 * function that get function reference and invoke one
 * assignment - show alert dialogs of any type
 */

fun Fragment.alert(factory : AlertBuilder.() -> Unit) = this.activity?.alert(factory)
fun View.alert(factory : AlertBuilder.() -> Unit) = this.context?.alert(factory)
fun Context.alert(factory : AlertBuilder.() -> Unit)  {

    fun alertFactory(
            title : String?,
            message : String?,
            okRes : Int? = R.string.ok,
            cancelRes : Int? = R.string.cancel,
            result : (Boolean) -> Unit,
            viewList: List<View>? ) {  AlertDialog.Builder(this)
            .also { builder -> builder.setTitle(title) }
            .setMessage(message)
            .also { builder -> okRes?.let { builder.setPositiveButton(okRes, { _, _ -> result.invoke(true) }) } }
            .also { builder -> cancelRes?.let { builder.setNegativeButton(cancelRes, { _, _ ->  result.invoke(false) }) } }
            .also { builder -> viewList?.let {
                it.forEach {
                    builder.setView(it) } } }
            .create()
            .also { it.window.attributes.windowAnimations = R.style.TaskAlertDialogs}
            .show()
    }

    val alertBuilder = AlertBuilder().apply(factory)
    Observable.create<Boolean>( { observable ->
        val message: String = when (alertBuilder.message) {
            is Int -> getString(alertBuilder.message as Int)
            is String -> alertBuilder.message as String
            else -> throw TextResourceException()
        }
        alertFactory(
                title = alertBuilder.title,
                message = message,
                okRes = alertBuilder.okRes,
                cancelRes = alertBuilder.cancelRes,
                result = { result : Boolean ->
                    if (result) {
                        alertBuilder.onResult?.invoke()
                                ?.let { observable.onNext(true) } }
                    observable.onComplete() },
                viewList = alertBuilder.viewList)
    } ).subscribe()
}


open class TextResourceException(message: String = "Set text only String or ResId resource!") : RuntimeException(message)