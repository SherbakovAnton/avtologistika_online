package com.team.estafeta.avtologistika_online.model.db.entity

import com.team.estafeta.avtologistika_online.model.db.realmCore.DataEntity
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by antony on 19.04.18.
 */
@RealmClass open  class KpiHistory : DataEntity {

    @PrimaryKey
    var id : Int? = 1

    var lastUpdate : Long? = null

    companion object {

        fun init() : KpiHistory {
            return KpiHistory().apply {
                lastUpdate = Calendar.getInstance().apply { time = Date() }.timeInMillis
            }
        }
    }

    override fun toString() = asString
}

fun KpiHistory.toDate() : String {
    return SimpleDateFormat("dd.MM HH:mm").format(Date(this.lastUpdate?.toLong()!!))

}



