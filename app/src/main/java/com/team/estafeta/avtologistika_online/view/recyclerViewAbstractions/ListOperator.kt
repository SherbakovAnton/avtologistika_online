package com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions

/**
 * Created by antony on 05.04.18.
 */

interface ListOperator<in T> {

    fun addItem(stub : T)

    fun addItems(stubs : List<T>)

    fun setItems(stubs : List<T>)

    fun removeItem(stub : T)

    fun removeAll()

}