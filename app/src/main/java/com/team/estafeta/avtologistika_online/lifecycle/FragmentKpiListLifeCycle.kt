package com.team.estafeta.avtologistika_online.lifecycle

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.team.estafeta.avtologistika_online.extension.observe
import com.team.estafeta.avtologistika_online.view.fragment.FragmentKpiList
import io.reactivex.disposables.Disposable

/**
 * Created by antony on 12.04.18.
 */

class FragmentKpiListLifeCycle(private val fragment : FragmentKpiList) : LifecycleObserver {

    private var disposable : Disposable? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME) fun onResume() {
        fragment.run {
            progressState = true
            disposable = kpiListPresenter.kpiList?.subscribe(::onReceiveKpiList)
            observe(dragAndDropViewModel.onUpdateKpiList) { it?.let { list -> onReceiveKpiList(list) } }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE) fun onPause() {
        fragment.progressState = false
        disposable?.dispose()
    }

}