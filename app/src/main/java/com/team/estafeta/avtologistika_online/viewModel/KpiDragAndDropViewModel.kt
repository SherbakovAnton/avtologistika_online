package com.team.estafeta.avtologistika_online.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.team.estafeta.avtologistika_online.WidgetApp
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import com.team.estafeta.avtologistika_online.model.db.repository.setKpiOnPanelAndSave
import com.team.estafeta.avtologistika_online.widget.WidgetManager

/**
 * Created by antony on 19.04.18.
 */
class KpiDragAndDropViewModel : ViewModel() {

    val onUpdateKpiList = MutableLiveData<List<Kpi>>()

    fun onKpiDragged(kpiProperties : Pair<Kpi, Int>) {
        kpiProperties.first.setKpiOnPanelAndSave(kpiProperties.second) {
            onUpdateKpiList.postValue(it)
            notifyWidget()
        }
    }

    private fun notifyWidget() {
        WidgetManager.updateInfo(WidgetApp.appContext())
    }
}