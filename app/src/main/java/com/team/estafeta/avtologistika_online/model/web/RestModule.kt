package com.team.estafeta.avtologistika_online.model.web

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.team.estafeta.avtologistika_online.BuildConfig
import com.team.estafeta.avtologistika_online.model.db.entity.User
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.estafeta.core.tracking.realm.getFirstEntity
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by antony on 04.04.18.
 */

object Web {

    fun getToken(login : String, password : String) = retrofitBase.getToken(login, password)
    fun getCompanies() = retrofitAuth.getCompanies()
    fun getListKpi() = retrofitAuth.getListKpi()
    fun getListKpiValues(body : List<Api.GetKpiRequest>) = retrofitAuth.getKpiValues(body)

    private val timeOut = 30L
    private val period = TimeUnit.SECONDS

    private val retrofitBase by lazy { buildRetrofit(::buildBaseHttpClient) }
    private val retrofitAuth by lazy { buildRetrofit(::buildBaseHttpClient) }

    private fun buildBaseHttpClient() =
            OkHttpClient()
                    .newBuilder()
                    .addInterceptor { chain: Interceptor.Chain ->
                        val originalRequest = chain.request()
                        val builder = originalRequest.newBuilder().apply {
                            User::class.getFirstEntity()?.let { this.header("Authorization", it.auth()) }
                        }

                        val newRequest = builder.build()
                        chain.proceed(newRequest)
                    }
                    .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
                    .connectTimeout(timeOut, period)
                    .readTimeout(timeOut, period)
                    .build()

    private fun buildRetrofit(clientFoo: () -> OkHttpClient) : Api =
            Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gsonTransformer))
                    .baseUrl(BuildConfig.testUrl)
                    .client(clientFoo())
                    .build()
                    .create(Api::class.java)


    private val gsonTransformer = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
            .setLenient()
            .create()

}




