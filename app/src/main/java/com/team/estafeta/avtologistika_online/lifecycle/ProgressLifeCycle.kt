package com.team.estafeta.avtologistika_online.lifecycle

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.team.estafeta.avtologistika_online.view.fragment.ProgressDialog

/**
 * Created by antony on 23.04.18.
 */
class ProgressLifeCycle(private val dialog : ProgressDialog) : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY) fun onDestroy() {
        dialog.progressDialogHide()
    }

}