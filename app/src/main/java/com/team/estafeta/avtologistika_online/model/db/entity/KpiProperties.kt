package com.team.estafeta.avtologistika_online.model.db.entity

import com.team.estafeta.avtologistika_online.model.db.realmCore.DataEntity
import io.realm.annotations.RealmClass


/**
 * Created by antony on 17.04.18.
 */

@RealmClass open class KpiProperties : DataEntity {

    var index : Int = -1

    var uiPosition : Int = KpiUiPosition.inMainList.ordinal
        private set

    fun setUiPosition(position: KpiUiPosition) {
        uiPosition = position.ordinal
    }

    enum class KpiUiPosition {
        inMainList,
        inWidget;
    }

    override fun toString() = asString
}