package com.team.estafeta.avtologistika_online.model.db.repository

import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.estafeta.core.tracking.realm.getAll
import org.estafeta.core.tracking.realm.save
import org.estafeta.core.tracking.realm.saveAll


/**
 * Created by antony on 17.04.18.
 */

fun getAllKpi() =  Kpi::class.getAll()

fun Kpi.setKpiOnPanelAndSave(index : Int, asyncBlock : (List<Kpi>?) -> Unit) {

    Observable.create(ObservableOnSubscribe<List<Kpi>?> {
            this::class.getAll()
                    ?.filter { it.properties?.index == index }
                    ?.forEach {
                        it.resetProperties()
                        it.save()
                    }
            setProperties(index).save()

            val list = getAllKpi()
            asyncBlock(list)
    }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
}

fun List<Kpi>.mergeWithDb(asyncBlock : (List<Kpi>) -> Unit) {
    Observable.create(ObservableOnSubscribe<List<Kpi>> {
        val mergedList : List<Kpi>? = Kpi::class.getAll()
                ?.mapNotNull { db -> db.merge(find { it.id == db.id }) }
                ?.let {
                    it.saveAll()
                    it }
                ?:let {
                    this.saveAll()
                    this }
            asyncBlock(mergedList ?: this)

    }).subscribe()
}



fun getSelectedKpiList(asyncBlock : (List<Kpi>?) -> Unit) {
    Observable.create(ObservableOnSubscribe<List<Kpi>> { emitter ->
        Kpi::class.getAll()
                ?.filter { it.properties != null }
                .also {
                    if (it != null && !it.isEmpty()) emitter.onNext(it)
                    else emitter.onError(NullPointerException())
                }
    }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ asyncBlock(it) }, { asyncBlock(null) })
}

fun List<Kpi>.filterKpiWithNullProperties(asyncBlock : (List<Kpi>?) -> Unit) {
    Observable.create(ObservableOnSubscribe<List<Kpi>?> { emitter ->
                        filter { it.properties == null }
                        .also (emitter::onNext)
    }).observeOn(AndroidSchedulers.mainThread())
            .subscribe({ asyncBlock(it) })
}

fun List<Kpi>.filterSelectedKpiList(asyncBlock : (List<Kpi>?) -> Unit) {
    Observable.create(ObservableOnSubscribe<List<Kpi>?> { emitter ->
                        filter { it.properties != null }
                        .also (emitter::onNext)
    }).observeOn(AndroidSchedulers.mainThread())
            .subscribe({ asyncBlock(it) })
}