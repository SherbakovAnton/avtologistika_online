package com.team.estafeta.avtologistika_online.dataprovider

import com.team.estafeta.avtologistika_online.R.string.no_internet_connection
import com.team.estafeta.avtologistika_online.WidgetApp
import com.team.estafeta.avtologistika_online.extension.toast
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import com.team.estafeta.avtologistika_online.model.db.repository.mergeWithDb
import com.team.estafeta.avtologistika_online.model.db.repository.saveKpiHistoryTimeStamp
import com.team.estafeta.avtologistika_online.model.web.Api
import com.team.estafeta.avtologistika_online.model.web.Web
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.androidannotations.annotations.EBean
import org.estafeta.core.tracking.realm.getAll
import org.jetbrains.anko.doAsync
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by antony on 19.04.18.
 */

@EBean(scope = EBean.Scope.Singleton) open class KpiDataProvider {

    private val isServerConnected : Boolean = true

    /********************************************* API ********************************************/

    fun updateAllKpi() = getKpiListFromServer(null)

    fun getKpiList(observer : Observer<List<Kpi>>) = loadData(observer, ::getKpiListFromServer, ::getKpiListFromDB)

    /******************************************* PRIVATE ******************************************/

    fun getKpiListFromDB(observer : Observer<List<Kpi>>) {

        Observable.create(ObservableOnSubscribe<List<Kpi>> {
            Kpi::class.getAll()?.let { observer.onNext(it) }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    fun getKpiListFromServer(observer : Observer<List<Kpi>>?) {
        Web.getListKpi()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(3)
                .subscribe ({ getKpiValues(observer, it) }, ::handleError)
    }

    private fun getKpiValues(observer : Observer<List<Kpi>>?,  list : List<Kpi>) {
        val kpiMap : MutableMap<String?, Kpi?> = list.fold(HashMap(),
                { accumulator, item ->
                    accumulator.put(item.id, item)
                    accumulator
                })

        Web.getListKpiValues(Api.GetKpiRequest.buildRequestArray(list))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(3)
                .subscribe ( { listFromServer ->
                    listFromServer.map { it.apply { merge(kpiMap[it.id]) } }
                                .mergeWithDb { mergedList ->
                                    mergedList.also { observer?.onNext(mergedList) }
                                              .also { saveKpiHistoryTimeStamp() }
                                }

                }, ::handleError )
    }

    private fun loadData(observer : Observer<List<Kpi>>,
                         fromServer : (observable: Observer<List<Kpi>>) -> Unit,
                         fromDataBase : (observable: Observer<List<Kpi>>) -> Unit) {
        if (isServerConnected) doAsync {
            fromDataBase(observer)
            fromServer(observer)
        } else doAsync { fromDataBase(observer) }

    }

    private fun handleError(it : Throwable) {

        when (it.cause) {
            is UnknownHostException,
            is SocketTimeoutException -> WidgetApp.appContext().toast(no_internet_connection)
            else -> WidgetApp.appContext().toast(it.localizedMessage!! + " exception...")
        }
    }
}




