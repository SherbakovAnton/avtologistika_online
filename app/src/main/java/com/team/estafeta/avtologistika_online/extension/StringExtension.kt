package com.team.estafeta.avtologistika_online.extension

import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat

/**
 * Created by antony on 06.04.18.
 */

fun String?.validate(isNotValidFunc : () -> Unit) {
    if (this.isNullOrEmpty()) isNotValidFunc()
}

fun String.toHHmm() : String {
    var time = LocalTime(0, 0) // midnight
    time = time.plusSeconds(this.toDoubleOrNull()?.toInt() ?: 0)
    return DateTimeFormat.forPattern("HH:mm").print(time)
}
