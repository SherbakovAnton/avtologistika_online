@file:JvmName("RealmModelImpl")
@file:JvmMultifileClass
@file:Suppress("UNCHECKED_CAST")

package org.estafeta.core.tracking.realm

import android.util.Log
import io.realm.*
import kotlin.reflect.KClass

/**
 * Created by SherbakovAnton (1mailanton@gmail.com) on 08.09.17.
 *
 * Function set created for interaction with Realm DataBase for
 *  - Kotlin
 *  - Java
 *  All functions are synchronous
 */

/**
 * Query - high order function, that perform Query to Realm DataBase
 * and return Unit(in java perform void function)
 */
internal typealias Query<T> = (RealmQuery<T>) -> Unit

/**
 * ****************************************GET FIRST************************************************
 */

/**
 * Kotlin function for getting the first entity from DataBase if exist
 * For example:
 * var entity : TestRealmEntity? = (TestRealmEntity::class).getFirstEntity()
 */
fun <T : RealmModel> KClass<T>.getFirstEntity(r: Realm = Realm.getDefaultInstance()): T? {
    return  getFirstEntityPrivate(this, r) as T?
}

/**
 * Java function for getting the first entity from DataBase if exist
 * For example:
 * TestRealmEntity entity = RealmModelImpl.getFirstEntity(TestRealmEntity.class)
 */
fun <T : RealmModel> getFirstEntity(clazz : Class<T>, r : Realm): T? {
    return  if (r == null) getFirstEntityPrivate(clazz.kotlin, Realm.getDefaultInstance()) as T?
            else getFirstEntityPrivate(clazz.kotlin, r) as T?
}

// Realisation
private fun <T : RealmModel> getFirstEntityPrivate(clazz : KClass<T>, r : Realm): RealmModel? {
    var entity : RealmModel? = null
    r.use { R : Realm ->
            R.executeTransaction { run {
                R.where(clazz.java).findFirst()?.let { first : RealmModel ->
                    entity = R.copyFromRealm(first)
                }
            }
        }
    }
    Log.v(TAG(), entity.toString())
    return entity
}

/**
 * ****************************************GET BY ID************************************************
 */

/**
 * Kotlin function for getting the entity by id from DataBase if exist
 * For example:
 * var entity : TestRealmEntity? = (TestRealmEntity::class).getById(id, realm)
 * if realm is null - get default instance
 */
fun <T : RealmModel> KClass<T>.getById(id: String?, r: Realm = Realm.getDefaultInstance()): T? =
        getByIdPrivate(id, this, r)

fun <T : RealmModel> KClass<T>.getById(id: Int?, r: Realm = Realm.getDefaultInstance()): T? =
        getByIdPrivate(id, this, r)

/**
 * Java function for getting the entity by id from DataBase if exist
 * For example:
 * TestRealmEntity entity = RealmModelImpl.getById(String id, TestRealmEntity.class, Realm realm)
 * if realm is null - get default instance
 */
fun <T : RealmModel> getById(id: String, clazz : Class<T>, r : Realm = Realm.getDefaultInstance()): T? =
        getByIdPrivate(id, clazz.kotlin, r)

fun <T : RealmModel> getById(id: String, clazz : Class<T>): T? =
        getByIdPrivate(id, clazz.kotlin, Realm.getDefaultInstance())

/**
 * Java function for getting the entity by id from DataBase if exist
 * For example:
 * TestRealmEntity entity = RealmModelImpl.getById(int id, TestRealmEntity.class, Realm realm)
 * if realm is null - get default instance
 */
fun <T : RealmModel> getById(id: Int, clazz : Class<T>, r : Realm = Realm.getDefaultInstance()): T? =
        getByIdPrivate(id, clazz.kotlin, r)

fun <T : RealmModel> getById(id: Int, clazz : Class<T>): T? =
        getByIdPrivate(id, clazz.kotlin, Realm.getDefaultInstance())

// Realisation
private fun <T : RealmModel> getByIdPrivate(id: String?, clazz : KClass<T>, r : Realm): T? {

    if (id == null || id.isEmpty()) return null
    var obj: T? = null
    r.use { r.where(clazz.java).equalTo("id", id).findFirst()?.also { obj = r.copyFromRealm(it) } }
    return obj
}

private fun <T : RealmModel> getByIdPrivate(id: Int?, clazz : KClass<T>, r : Realm): T? {
    if (id == null || id == 0) return null
    var obj: T? = null
    r.use { r.where(clazz.java).equalTo("id", id).findFirst()?.also { obj = r.copyFromRealm(it) } }
    return obj
}

/**
 * *****************************************GET ALL*************************************************
 */

/**
 * Kotlin function for getting all entities from Da taBase if exist
 * For example:
 * var entities =
 */
fun <T : RealmModel> KClass<T>.getAll(r: Realm? = Realm.getDefaultInstance()) : List<T>? {
    return if (r == null) getAllPrivate(this, Realm.getDefaultInstance()) as List<T>?
           else  getAllPrivate(this, r) as List<T>?
}

/**
 * Java function for getting all entities from DataBase if exist
 */

fun <T : RealmModel> getAll(clazz: Class<T>, r : Realm) : List<T>? {

    return  if (r == null) getAllPrivate(clazz.kotlin, Realm.getDefaultInstance()) as List<T>
            else getAllPrivate(clazz.kotlin, r) as List<T>
}

private fun <T : RealmModel> getAllPrivate(clazz: KClass<T>, r : Realm) : List<RealmModel>? {
    var entities : List<RealmModel>? = null
    r.use { R : Realm ->
            R.executeTransaction {
                run {
                    R.where(clazz.java).findAll()?.let { it : List<RealmModel>? ->

                        val result = R.copyFromRealm(it)
                        if(!result.isEmpty()) entities = result
                }
            }
        }
    }
    return entities
}


/**
 * ********************************GET FIRST WITH QUERY (sorted)**************************************
 */

fun <T : RealmModel> KClass<T>.getFirstWithQuery(r: Realm = Realm.getDefaultInstance(), query: Query<T>) : T? {
    var result: T? = null
    r.use {
        r.forEntity(this.java).withQuery(query).findFirst()?.let {
            result = r.copyFromRealm(it)
        }
    }
    return result
}

/**
 * ********************************GET ALL WITH QUERY (sorted)**************************************
 */

/**
 * Kotlin function for getting all sorted entities from Da taBase if exist
 */
fun <T : RealmModel> KClass<T>.getAllSorted(fieldName: String,
                                            order: Sort = Sort.ASCENDING,
                                            query: Query<T>? = null,
                                            preDetach: ((RealmResults<T>, Realm) -> RealmResults<T>)? = null,
                                            realm: Realm = Realm.getDefaultInstance()): List<T> {
    realm.use { r ->
        val q = r.forEntity(this.java)
        query?.let { q.withQuery(it) }
        var result = q.findAll().sort(fieldName, order)
        preDetach?.let { result = it.invoke(result, r) }
        return r.copyFromRealm(result)
    }
}

/**
 * ******************************** CLEAR REALM *********************************************
 */

fun clearRealm(r : Realm = Realm.getDefaultInstance()) {
    r.use { R : Realm ->
        R.executeTransaction { R.deleteAll() }
    }
}

/**
 * ********************************GET MAX MIN*********************************************
 */

fun <T : RealmModel> KClass<T>.getMax(fieldName: String, query: Query<T>? = null): Number? =
        Realm.getDefaultInstance().use {
            val q = it.forEntity(this.java)
            query?.let { q.withQuery(it) }
            q.max(fieldName)
        }

fun <T : RealmModel> Class<T>.getMax(fieldName: String, query: Query<T>? = null): Number? =
        Realm.getDefaultInstance().use {
            val q = it.forEntity(this)
            query?.let { q.withQuery(it) }
            q.max(fieldName)
        }

fun <T : RealmModel> KClass<T>.getMin(fieldName: String, query: Query<T>? = null): Number? =
        Realm.getDefaultInstance().use {
            val q = it.forEntity(this.java)
            query?.let { q.withQuery(it) }
            q.min(fieldName)
        }

fun <T : RealmModel> Class<T>.getMin(fieldName: String, query: Query<T>? = null): Number? =
        Realm.getDefaultInstance().use {
            val q = it.forEntity(this)
            query?.let { q.withQuery(it) }
            q.min(fieldName)
        }

/**
 * *****************************************GET ID**************************************************
 */

/**
 * Kotlin function for getting MIN id for object
 * For example:
 * val minId: Int = Foo::class.getMinId()
 * if object null return 0
 */
fun <T : RealmModel> KClass<T>.getMinId(): Int = getMinIdPrivate(this)

/**
 * Java function for getting MIN id for object
 * For example:
 * int minId = RealmModelImpl.getMinId(Foo.class);
 * if object null return 0
 */
fun <T : RealmModel> getMinId(clazz: Class<T>): Int = getMinIdPrivate(clazz.kotlin)

/**
 * Implementation
 * */
private fun <T : RealmModel> getMinIdPrivate(clazz: KClass<T>): Int =
        Realm.getDefaultInstance().where(clazz.java).min("id")?.toInt() ?: 0

/**
 * Kotlin function for make id for object
 * For example:
 * val newId: Int = Foo::class.getNewId()
 * if object null return 1
 */
fun <T : RealmModel> KClass<T>.getNewId(): Int = getNewIdPrivate(this)

/**
 * Java function for make id for object
 * For example:
 * int newId = RealmModelImpl.getNewId(Foo.class);
 * if object null return 1
 */
fun <T : RealmModel> getNewId(clazz: Class<T>): Int = getNewIdPrivate(clazz.kotlin)

/**
 * Implementation
 * */
private fun <T : RealmModel> getNewIdPrivate(clazz: KClass<T>): Int =
        Realm.getDefaultInstance().where(clazz.java).max("id")?.toInt()?.let { it + 1 } ?: 1

/**
 * *******************************************SAVE**************************************************
 */

/**
 * Kotlin function creates a new entity in database or updates an existing one.
 * If entity has no Primary Key -> creates a new entity,
 * if has Primary Key           -> updates an existing one.
 */
inline infix fun <T : RealmModel> T.save_to(r: Realm): T? = saveEntityPrivate(this, r)

inline fun <T : RealmModel> T.save(r: Realm = Realm.getDefaultInstance()): T? = saveEntityPrivate(this, r)

/**
 * Java function creates a new entity in database or updates an existing one.
 * If entity has no Primary Key -> creates a new entity,
 * if has Primary Key           -> updates an existing one.
 */
@JvmName(name = "for_java")
fun <T : RealmModel> save_to(obj: T, r: Realm) { saveEntityPrivate(obj, r) }

inline fun <T : RealmModel> saveEntityPrivate(obj: T, r: Realm): T? {
    r.use { realm ->
        realm.beginTransaction()
        val result = if (obj.hasPrimaryKey(realm)) realm.copyToRealmOrUpdate(obj)
        else realm.copyToRealm(obj)
        realm.commitTransaction()
        return realm.copyFromRealm(result)
    }
}

/**
 * ****************************************SAVE ALL*************************************************
 */

/**
 * Kotlin function save_to Collection of Realm Entities.
 * For each entity:
 * if entity has no Primary Key -> creates a new entity,
 * if has Primary Key           -> updates an existing one.
 */
inline fun <T : List<RealmModel>> T.saveAll(r : Realm = Realm.getDefaultInstance()) {
    saveAllPrivate(r,this)
}

/**
 * Java function save_to Collection of Realm Entities.
 * For each entity:
 * if entity has no Primary Key -> creates a new entity,
 * if has Primary Key           -> updates an existing one.
 */
@JvmName(name = "for_java")
inline fun <T : List<out RealmModel>> saveAll(r : Realm, entities : List<T>) where T : RealmModel { saveAllPrivate(r, entities) }

//Realisation
inline fun saveAllPrivate(r : Realm, entities : List<RealmModel>) {
    r.transaction { R : Realm ->
        entities.forEach { it
            if (it.hasPrimaryKey(R)) R.copyToRealmOrUpdate(it)
            else R.copyToRealm(it) }
    }
}

/**
 * ***************************************DELETE ALL************************************************
 */

/**
 * Kotlin function to delete all entries of this type in database
 */
fun <T : RealmModel> KClass<T>.deleteAll(r: Realm) { deleteAllPrivate(this.java, r) }

/**
 * Java function to delete all entries of this type in database
 */
@JvmName(name = "for_java_deleteAll")
fun <T : RealmModel> deleteAll(clazz: Class<T>, r: Realm) { deleteAllPrivate(clazz, r) }

private fun <T : RealmModel> deleteAllPrivate(clazz: Class<T>, r: Realm) {
    r.use { realm -> realm.transaction { it.forEntity(clazz).findAll().deleteAllFromRealm() } }
}

/**
 * ***************************************DELETE****************************************************
 */

/**
 * Kotlin function to delete entry from database
 */
fun <T : RealmModel> T.deleteById(id: Int, r: Realm = Realm.getDefaultInstance()) { deleteByIdPrivate(this, id, r) }

fun <T : RealmModel> T.deleteById(id: String, r: Realm = Realm.getDefaultInstance()) { deleteByIdPrivate(this, id, r) }

/**
 * Java function to delete entry from database
 */
@JvmName(name = "for_java_delete")
fun <T : RealmModel> deleteById(obj: T, id: Int, r: Realm) { deleteByIdPrivate(obj, id, r) }

private fun <T : RealmModel> deleteByIdPrivate(obj: T, id: Int, r: Realm) {
    r.use { realm -> realm.transaction { realm.forEntity(obj).equalTo("id", id).findAll().deleteAllFromRealm() } }
}

private fun <T : RealmModel> deleteByIdPrivate(obj: T, id: String, r: Realm) {
    r.use { realm -> realm.transaction { realm.forEntity(obj).equalTo("id", id).findAll().deleteAllFromRealm() } }
}

/**
 * ******************************************QUERY**************************************************
 */


/**
 * Query to the database with RealmQuery instance as argument
 */
fun <T : RealmModel> KClass<T>.query(R : Realm = Realm.getDefaultInstance(), query: Query<T>) : List<T> {
    R.use { realm ->
        val result = realm.forEntity(this.java).withQuery(query).findAll()
        return realm.copyFromRealm(result)
    }
}

/**
 * ************************************UTILITY METHODS**********************************************
 */
private fun <T : RealmModel> Realm.forEntity(instance: T): RealmQuery<T>  = this.where(instance.javaClass)


private fun <T : RealmModel> Realm.forEntity(clazz: Class<T>): RealmQuery<T> = this.where(clazz)

private fun <T> T.withQuery(block: (T) -> Unit): T {
    block(this)
    return this }


fun Realm.transaction(action: (Realm) -> Unit) {
    use { executeTransaction { action(this) } }
}

/**
 * Function checks if Realm Entity has some field with annotation @PrimaryKey
 * If Realm Entity is not part of the schema for this Realm -> throws IllegalArgumentException
 * Check did you added realm-android plugin in your build.gradle file?
 */
fun <T : RealmModel> T.hasPrimaryKey(realm: Realm): Boolean {
    if(realm.schema.get(this.javaClass.simpleName) == null){
        throw IllegalArgumentException(this.javaClass.simpleName +
                " is not part of the schema for this Realm. Did you added realm-android plugin in your build.gradle file?")
    }
    return realm.schema.get(this.javaClass.simpleName)!!.hasPrimaryKey()
}

fun <T : RealmModel> T.copyFromRealm(realm: Realm = Realm.getDefaultInstance()): T? {
    var result: T? = null
    realm.use { result = it.copyFromRealm(this) }
    return result
}

//private fun tag() = "[Class - ${this.javaClass.canonicalName} , Line - ${Thread.currentThread().stackTrace[2].methodName.toString()}]"
private fun TAG() = "[File - RealmModelImpl]"


interface CreateQuery<T : RealmModel?> {
    fun createQuery(rq : RealmQuery<T>) : Unit
}

