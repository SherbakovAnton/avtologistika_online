package com.team.estafeta.avtologistika_online.model.db.repository

import com.team.estafeta.avtologistika_online.WidgetApp
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import com.team.estafeta.avtologistika_online.model.db.entity.KpiHistory
import com.team.estafeta.avtologistika_online.widget.WidgetManager
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.estafeta.core.tracking.realm.save

/**
 * Created by antony on 19.04.18.
 */

fun saveKpiHistoryTimeStamp() {
    Observable.create(ObservableOnSubscribe<List<Kpi>?> {
        KpiHistory.init().save()
        WidgetManager.updateInfo(WidgetApp.appContext())
    }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
}