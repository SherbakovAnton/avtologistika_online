package com.team.estafeta.avtologistika_online.model.db.repository

import com.team.estafeta.avtologistika_online.extension.plus
import com.team.estafeta.avtologistika_online.model.db.entity.User
import com.team.estafeta.avtologistika_online.model.db.entity.UserState
import com.team.estafeta.avtologistika_online.model.entity.UserTemporary
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.estafeta.core.tracking.realm.getFirstEntity
import org.estafeta.core.tracking.realm.save

/**
 * Created by antony on 17.04.18.
 */

object UserRepository {

    fun getUser(asyncBlock : (User?) -> Unit) {
        Observable.create(ObservableOnSubscribe<User> { emitter ->
            User::class.getFirstEntity()
                    ?.let { userNotNull -> emitter.onNext(userNotNull) }
                    ?:let { emitter.onError(NullPointerException()) }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ asyncBlock(it) }, { asyncBlock(null) })
    }


    fun initFrom(user : UserTemporary) : User {
        return  User().apply {
            login = user.login
            password = user.password
            setState(UserState.logIn)
            token = user.primaryToken
            companyId?.let { user.companyId = it }
        }
    }

    fun setUserGotIt(isGotIt : Boolean = true) {
        Observable.create(ObservableOnSubscribe<Any> {
            User::class.getFirstEntity()?.also {
                it.settings?.isGotIt = isGotIt
                it.save()
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }

    fun isUserGotIt(asyncBlock : (Boolean) -> Unit) {
        Observable.create(ObservableOnSubscribe<Boolean> {
                   it.onNext(User::class.getFirstEntity()?.settings?.isGotIt ?: false)
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ asyncBlock(it) })
    }

}

fun User.saveUser(asyncBlock : (() -> Unit)? = null) {
    Observable.create(ObservableOnSubscribe<Any> {
        this.save() + it.onNext(true)
    }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ asyncBlock?.invoke() })
}


