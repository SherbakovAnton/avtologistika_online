package com.team.estafeta.avtologistika_online.widget

import android.appwidget.AppWidgetManager
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import org.androidannotations.annotations.EReceiver

@EReceiver open class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        AppWidgetManager.getInstance(context)
                .getAppWidgetIds(ComponentName(context, WidgetEstafeta::class.java))
                .also {
                    if (it.isNotEmpty()) WidgetScheduler.schedule(context)
        }
    }
}
