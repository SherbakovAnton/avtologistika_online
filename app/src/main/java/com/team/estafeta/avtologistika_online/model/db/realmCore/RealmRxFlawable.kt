package org.estafeta.core.tracking.realm


import android.os.HandlerThread
import android.os.Looper
import android.os.Process
import android.util.Log
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.FlowableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.realm.Realm
import io.realm.RealmModel
import io.realm.Sort

/**
 * Created by antonsherbakov on 06.03.18.
 */

internal const val threadName = "Scheduler-Realm-BackgroundThread"

/**
 * Query for all entities and observe changes returning a Flowable.
 */
fun <T : RealmModel> T.queryAllAsFlowable(managed : Boolean = false)
        = flowableQuery(managed = managed)
inline fun <reified T : RealmModel> queryAllAsFlowable(managed : Boolean = false)
        = flowableQuery<T>(managed = managed)


/**
 * Query for entities in database asynchronously and observe changes returning a Flowable.
 */
fun <T : RealmModel> T.queryAsFlowable(managed : Boolean = false, query: Query<T>)
        = flowableQuery(managed = managed, query = query)
inline fun <reified T : RealmModel> queryAsFlowable(managed : Boolean = false, noinline query: Query<T>)
        = flowableQuery(managed = managed, query = query)


/**
 * Query for sorted entities and observe changes returning a Flowable.
 */
fun <T : RealmModel> T.querySortedAsFlowable(fieldName: List<String>,
                                             order: List<Sort>,
                                             managed : Boolean = false,
                                             query: Query<T>? = null)
        = flowableQuery(fieldName, order, managed, query)
inline fun <reified T : RealmModel> querySortedAsFlowable(fieldName: List<String>,
                                                          order: List<Sort>,
                                                          managed : Boolean = false,
                                                          noinline query: Query<T>? = null)
        = flowableQuery(fieldName, order, managed, query)


/**
 * Query for sorted entities and observe changes returning a Flowable.
 */
fun <T : RealmModel> T.querySortedAsFlowable(fieldName: String,
                                             order: Sort,
                                             managed : Boolean = false,
                                             query: Query<T>? = null)
        = flowableQuery(listOf(fieldName), listOf(order), managed, query)
inline fun <reified T : RealmModel> querySortedAsFlowable(fieldName: String,
                                                          order: Sort,
                                                          managed : Boolean = false,
                                                          noinline query: Query<T>? = null)
        = flowableQuery(listOf(fieldName), listOf(order), managed, query)


/**
 * INTERNAL FUNCTIONS
 */
private fun <T : RealmModel> T.flowableQuery(fieldName: List<String>? = null, order: List<Sort>? = null,
                                             managed : Boolean = false,
                                             query: Query<T>? = null)
        = performFlowableQuery(fieldName, order, managed, query, this.javaClass)

@PublishedApi internal inline fun <reified T : RealmModel> flowableQuery(fieldName: List<String>? = null,
                                                                         order: List<Sort>? = null,
                                                                         managed : Boolean = false,
                                                                         noinline query: Query<T>? = null)
        = performFlowableQuery(fieldName, order, managed, query, T::class.java)

@PublishedApi internal fun <T:RealmModel> performFlowableQuery(fieldName: List<String>? = null,
                                                               order: List<Sort>? = null,
                                                               managed : Boolean = false,
                                                               query: Query<T>? = null,
                                                               javaClass : Class<T>) : Flowable<List<T>> {
    return prepareObservableQuery(javaClass, { realm, subscriber ->
        val realmQuery = realm.where(javaClass)
        query?.invoke(realmQuery)

        val result = if (fieldName != null && order != null) {
            realmQuery.findAllSortedAsync(fieldName.toTypedArray(), order.toTypedArray())
        } else {
            realmQuery.findAllAsync()
        }

        result.asFlowable()
                .filter { it.isLoaded }
                .map { if(!managed) realm.copyFromRealm(it) else it }
                .subscribe({ subscriber.onNext(it) }, { subscriber.onError(it) })
    })
}



private inline fun <D : RealmModel, T : Any> prepareObservableQuery(clazz : Class<D>,
                                                                    crossinline closure : (Realm, FlowableEmitter<in T>) -> Disposable) : Flowable<T> {
    var realm : Realm? = null
    var mySubscription : Disposable? = null

    var backgroundThread  : HandlerThread? = null
    val looper : Looper = if (Looper.myLooper() == null) {
        backgroundThread = BackgroundThread()
        backgroundThread.start()
        backgroundThread.looper
    } else Looper.getMainLooper()


    return Flowable.defer {
        Flowable.create(FlowableOnSubscribe<T> {
            realm = getRealmInstance(clazz)
            mySubscription = closure(realm!!, it)
        }, BackpressureStrategy.BUFFER)
                .doOnCancel {
                    Log.d("RealmRX", "realm?.close()")
                    realm?.close()
                    mySubscription?.dispose()
                    backgroundThread?.interrupt()
                }
                .unsubscribeOn(AndroidSchedulers.from(looper))
                .subscribeOn(AndroidSchedulers.from(looper))
    }
}

internal class BackgroundThread : HandlerThread(threadName, Process.THREAD_PRIORITY_BACKGROUND)

internal fun getLooper() : Looper? =
    if (isMainLooper().not()) HandlerThread(threadName, Process.THREAD_PRIORITY_BACKGROUND).apply { start() }.looper
    else Looper.getMainLooper()

private fun isMainLooper() = Looper.myLooper() == Looper.getMainLooper()
