package com.team.estafeta.avtologistika_online.model.db.realmCore

import io.realm.RealmModel
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle
import java.io.Serializable

/**
 * Created by antony on 16.04.18.
 */
interface DataEntity : RealmModel, Serializable {
    val asString: String
        get() = ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE)!!

    override fun toString(): String
}
