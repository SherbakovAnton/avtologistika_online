package com.team.estafeta.avtologistika_online.extension

import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import org.apache.commons.lang3.StringUtils

/**
 * Created by antony on 19.04.18.
 */

fun Kpi.measuringSystem() : String {
    return when (this.getMeasurementType()) {
    //MeasurementType.sec     -> { string(R.string.measure_sec)   }
        Kpi.MeasurementType.km      -> { string(R.string.measure_km)    }
        Kpi.MeasurementType.car     -> { string(R.string.measure_car)   }
        Kpi.MeasurementType.truck   -> { string(R.string.measure_truck) }
        else -> StringUtils.EMPTY
    }
}

fun Kpi.getAnimation() : String {

    return when (this.getMeasurementType()) {
        Kpi.MeasurementType.sec     -> { "animation_time.json" }
        Kpi.MeasurementType.km      -> { "animation_distance.json" }
        Kpi.MeasurementType.car,
        Kpi.MeasurementType.truck   -> { "animation_truck.json" }
        else -> "animation_distance.json"
    }
}

fun Kpi.needReverse(block : () -> Unit)  {

    return when (this.getMeasurementType()) {

        Kpi.MeasurementType.truck,
        Kpi.MeasurementType.car   -> { block() }
        else -> {}
    }
}