package com.team.estafeta.avtologistika_online.view.activity

import android.support.v7.app.AppCompatActivity
import android.view.Window
import com.team.estafeta.avtologistika_online.extension.obtainViewModel
import com.team.estafeta.avtologistika_online.extension.openActivityAfterClosingCurrent
import com.team.estafeta.avtologistika_online.model.db.ErrorLoading
import com.team.estafeta.avtologistika_online.model.db.entity.User
import com.team.estafeta.avtologistika_online.model.db.entity.UserState
import com.team.estafeta.avtologistika_online.viewModel.PresenterKpiList
import com.team.estafeta.avtologistika_online.viewModel.UserViewModel
import org.androidannotations.annotations.AfterInject
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.WindowFeature

/**
 * Created by antony on 24.04.18.
 */

@WindowFeature(Window.FEATURE_NO_TITLE)
@EActivity
open class StartActivity : AppCompatActivity() {

    @Bean lateinit var kpiListPresenter : PresenterKpiList

    @AfterInject fun onInit() {
        obtainViewModel(UserViewModel::class)
                .apply { dataBaseUser.subscribe(::onGetUser); errorLoad.subscribe(::noUser) }
    }

    private fun onGetUser(user : User) {
        when (user.getState()) {
            UserState.companySelected -> {
                kpiListPresenter.getListKpiAfterCompanyIsSelected()
                openActivityAfterClosingCurrent(KpiActivity_::class) }
            UserState.logOut,
            UserState.logIn -> openActivityAfterClosingCurrent(MainActivity_::class)
        }
    }

    private fun noUser(err : ErrorLoading) = openActivityAfterClosingCurrent(MainActivity_::class)
}