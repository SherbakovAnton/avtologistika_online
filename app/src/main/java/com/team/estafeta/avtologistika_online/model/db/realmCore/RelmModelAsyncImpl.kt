@file:JvmName("RealmModelAsyncImpl")
@file:JvmMultifileClass

package org.estafeta.core.tracking.realm

import android.os.Handler
import android.os.Looper
import io.realm.*
import kotlin.reflect.KClass

/**
 * Created by SherbakovAnton (1mailanton@gmail.com) on 12.09.17.
 *
 * Function set created for interaction with Realm DataBase for
 *  - Kotlin
 *  - Java
 *  All functions are asynchronous
 */

typealias UnitCallback = () -> Unit
typealias SingleCallback<T> = (T?) -> Unit
typealias PluralCallback<T> = (List<T>) -> Unit
typealias QueryAsync<T> = (RealmQuery<T>) -> Unit

// For JAVA
interface RealmEntityListCallBack<in T : RealmModel> {
    fun onEntityLoaded(list: List<T>?)
}

interface RealmEntityCallBack<in T: RealmModel> {
    fun onEntityLoaded(list: T?)
}

/**
 * ***********************************GET FIRST ASYNCHRONOUS****************************************
 */

/**
 * Kotlin function which returns last entity in database asynchronously.
 */
fun <T : RealmModel> KClass<T>.queryFirstAsync(callback : SingleCallback<T>) {
    mainThread {
        val R = Realm.getDefaultInstance()
        val result = R.where(this.java).findFirstAsync()
        RealmObject.addChangeListener(result) { it ->
            callback(if (it != null && RealmObject.isValid(it)) R.copyFromRealm(it) else null)
            RealmObject.removeAllChangeListeners(it)
            R.close()
        }
    }
}

/**
 * Java function which returns last entity in database asynchronously.
 */
fun <T : RealmModel> queryFisrtAsync(clazz : Class<T>, callback: RealmEntityCallBack<T>) {
    mainThread {
        val R = Realm.getDefaultInstance()
        val result = R.where(clazz).findFirstAsync()
        RealmObject.addChangeListener(result) { it : RealmModel ->
            callback.onEntityLoaded(if (it != null && RealmObject.isValid(it)) R.copyFromRealm(it) as T? else null)
            RealmObject.removeAllChangeListeners(it)
            R.close()
        }
    }
}

/**
 * ***********************************SAVE ALL ASYNCHRONOUS*****************************************
 */
fun <T : List<RealmModel>> T.saveAllAsync(r : Realm, callback: UnitCallback) = saveAllAsync(r, this, callback)

fun saveAllAsync(r : Realm, entities : List<RealmModel>, callback: UnitCallback) {
    mainThread {
        r.transaction { R : Realm ->
            entities.forEach {
                if (it.hasPrimaryKey(R)) R.copyToRealmOrUpdate(it) else R.copyToRealm(it)
                callback.invoke()
            }
        }
    }
}

/**
 * ***********************************GET ALL ASYNCHRONOUS******************************************
 */

/**
 * Kotlin function which returns all entities in database asynchronously
 */
fun <T : RealmModel> KClass<T>.queryAllAsync(callback: PluralCallback<T>) {
    mainThread {
        val realm = Realm.getDefaultInstance()
        val result: RealmResults<T> = realm.where(this.java).findAllAsync()

        result.addChangeListener { it ->
            callback(realm.copyFromRealm(it))
            result.removeAllChangeListeners()
            realm.close()
        }
    }
}

/**
 * Java function which returns all entities in database asynchronously
 */
fun <T : RealmModel> queryAllAsync(clazz : Class<T>, callback: RealmEntityListCallBack<T>) {
    mainThread {
        val realm = Realm.getDefaultInstance()
        val result: RealmResults<T> = realm.where(clazz).findAllAsync()

        result.addChangeListener { it ->
            callback.onEntityLoaded(realm.copyFromRealm(it))
            result.removeAllChangeListeners()
            realm.close()
        }
    }
}

/**
 * ***********************************ASYNCHRONOUS QUERY********************************************
 */

/**
 * Kotlin function which perform query for entities in database asynchronously.
 */
fun <T : RealmModel> KClass<T>.queryAsync(query: QueryAsync<T>, callback: PluralCallback<T>) {
    mainThread {
        val realm = Realm.getDefaultInstance()
        val realmQuery: RealmQuery<T> = realm.where(this.java)
        query(realmQuery)
        val result = realmQuery.findAllAsync()
        result.addChangeListener { it ->
            callback(realm.copyFromRealm(it))
            result.removeAllChangeListeners()
            realm.close()
        }
    }
}

/**
 * Java function which perform query for entities in database asynchronously.
 */
fun <T : RealmModel> queryAsync(clazz : Class<T>, query: QueryAsync<T>, callback: RealmEntityListCallBack<T>) {
    mainThread {
        val realm = Realm.getDefaultInstance()
        val realmQuery: RealmQuery<T> = realm.where(clazz)
        query(realmQuery)
        val result = realmQuery.findAllAsync()
        result.addChangeListener { it ->
            callback.onEntityLoaded(realm.copyFromRealm(it))
            result.removeAllChangeListeners()
            realm.close()
        }
    }
}

private fun mainThread(block: () -> Unit) {
    Handler(Looper.getMainLooper()).post(block)
}
