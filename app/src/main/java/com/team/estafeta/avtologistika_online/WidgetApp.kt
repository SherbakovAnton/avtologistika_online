package com.team.estafeta.avtologistika_online

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration
import org.androidannotations.annotations.EApplication

/**
 * Created by antony on 02.04.18.
 */
@EApplication open class WidgetApp : Application() {



    override fun onCreate() {
        super.onCreate()

        context = this

        Realm.init(this)
        val config = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .name("widget_data_base")
                .build()
        Realm.setDefaultConfiguration(config)
    }

    companion object {

        lateinit private var context : WidgetApp

        fun appContext() : WidgetApp = context
    }

}