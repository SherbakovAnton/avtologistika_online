package com.team.estafeta.avtologistika_online.model.entity

import java.io.Serializable

/**
 * Created by antony on 11.04.18.
 */

val lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."

data class ToolTip(
        val title : String?,
        val content : String?,
        val next : String?,
        val skip : String?) : Serializable {

    constructor() : this("Lorem", lorem, null, null)
}