package com.team.estafeta.avtologistika_online.lifecycle

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.team.estafeta.avtologistika_online.view.activity.MainActivity

/**
 * Created by antony on 03.04.18.
 */

class MainActivityLifeCycle(private val mainActivity : MainActivity) : LifecycleObserver {




    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {


    }
}