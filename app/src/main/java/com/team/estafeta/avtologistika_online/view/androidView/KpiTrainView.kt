package com.team.estafeta.avtologistika_online.view.androidView

import android.content.Context
import android.databinding.BindingAdapter
import android.util.AttributeSet
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import android.widget.TextView
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.getAnimation
import com.team.estafeta.avtologistika_online.extension.initialize
import com.team.estafeta.avtologistika_online.extension.measuringSystem
import com.team.estafeta.avtologistika_online.extension.needReverse
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import kotlinx.android.synthetic.main.kpi_train.view.*


/**
 * Created by antony on 13.04.18.
 */

open class KpiTrainView(context : Context?, attrs : AttributeSet?)
    : RelativeLayout(context, attrs) {

    val anim : Animation by lazy { AnimationUtils.loadAnimation(context, R.anim.text_kpi_value) }

    val kpiName         by lazy { findViewById<TextView>(R.id.textView5) }

    var trainState : TrainState = TrainState.up
    set(value) {
        field = value
        arrow.setImageResource(value.imageRes())
    }

    var onTrain : ((KpiTrainView.TrainState) -> Unit)? = null

    fun onTrainClick() {
        onTrain?.let {
            trainState = if(trainState == TrainState.up) TrainState.down else TrainState.up
            it(trainState) }
    }

    fun bindKpi(kpi : Kpi) {
        kpiName.text = kpi.name
        HARDCODED_METHOD(kpi)
    }


    private fun HARDCODED_METHOD(kpi : Kpi) {
        lottie.setAnimation(kpi.getAnimation())

        kpi.needReverse {
            lottie.repeatCount = Int.MAX_VALUE
            //lottie.repeatMode = ValueAnimator.REVERSE
        }

        //bottomKpiProperty.visibility = kpi.bottomKpiPropertyVisibility()

        when (kpi.getMeasurementType()) {
            Kpi.MeasurementType.sec     -> {

                fragmentKpiTimeContainer.visibility = View.VISIBLE

                fragmentKpiTimeHourValue.text = kpi.getValue().split(":")[0]
                fragmentKpiTimeMinValue.text = kpi.getValue().split(":")[1]

                fragmentKpiHours.text = getPluralsHours(kpi.getValue().split(":")[0])

            }
            Kpi.MeasurementType.km      -> {
                fragmentKpiDistanceMeasure.visibility = View.VISIBLE
                fragmentKpiDistanceValue.visibility = View.VISIBLE

                fragmentKpiDistanceValue.text = kpi.getValue()
                fragmentKpiDistanceMeasure.text = kpi.measuringSystem()
            }
            Kpi.MeasurementType.car,
            Kpi.MeasurementType.truck   -> {
                fragmentKpiCarsValue.visibility = View.VISIBLE
                fragmentKpiCarsValue.text = kpi.getValue()
            }
            else -> {
                fragmentKpiCarsValue.visibility = View.VISIBLE
                fragmentKpiCarsValue.text = kpi.getValue()
            }
        }
    }

    private fun getPluralsHours(input : String) : String {
        val inputInt : Int = try {
            Integer.parseInt(input)
        } catch (e : NumberFormatException) { Int.MAX_VALUE }
        return context.resources.getQuantityString(R.plurals.hours, inputInt)
    }



    enum class TrainState {
        up      { override fun imageRes() = R.drawable.arrow_down_icon },
        down    { override fun imageRes() = R.drawable.arrow_up_icon };

        abstract fun imageRes() : Int
    }

    init {
        initialize(R.layout.kpi_train)

        fragmentKpiValue.startAnimation(anim)

        val ta = context?.obtainStyledAttributes(attrs, R.styleable.InputView, 0, 0)
        try {
            val stateIndex = ta?.getInt(R.styleable.KpiTrainView_trainState, 0)
            trainState = TrainState.values()[stateIndex!!]
        } finally { ta?.recycle() }

        arrow.setOnClickListener {
            onTrainClick()
        }
    }
}

@BindingAdapter("injectKpi") fun KpiTrainView.onInjectKpi_(kpi: Kpi) {

    bindKpi(kpi)

}




