package com.team.estafeta.avtologistika_online.model.entity

import com.team.estafeta.avtologistika_online.model.db.entity.Company

/**
 * Created by antony on 04.04.18.
 */

data class UserTemporary(
        val login : String,
        val password : String) {

    var primaryToken : String? = null
    var companyId    : String? = null


    var companyList : List<Company>? = null

}