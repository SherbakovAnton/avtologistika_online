package com.team.estafeta.avtologistika_online.model.entity

/**
 * Created by antony on 12.04.18.
 */
data class UserSettings(val skin : Skin = Skin.White,
                        val widgetDisplay : Display = Display.Vertically,
                        val company : String? = "Renault Ukraine") {


    fun checkSkin(skin : Skin) : Boolean {
        return skin == Skin.White
    }

}

enum class Skin { White, Dark }

enum class Display { Vertically,Horizontally }