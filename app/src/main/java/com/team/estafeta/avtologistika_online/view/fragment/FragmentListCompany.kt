package com.team.estafeta.avtologistika_online.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v4.app.Fragment
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.observe
import com.team.estafeta.avtologistika_online.extension.plus
import com.team.estafeta.avtologistika_online.extension.toast
import com.team.estafeta.avtologistika_online.model.db.entity.Company
import com.team.estafeta.avtologistika_online.view.activity.KpiActivity_
import com.team.estafeta.avtologistika_online.view.androidView.CompanyItem
import com.team.estafeta.avtologistika_online.viewModel.LoginViewModel
import com.team.estafeta.avtologistika_online.viewModel.PresenterKpiList
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_company_list.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EFragment

/**
 * Created by antony on 04.04.18.
 */

@EFragment(R.layout.fragment_company_list) open class FragmentListCompany : Fragment() {

    @Bean lateinit var kpiListPresenter : PresenterKpiList

    companion object { fun newInstance() : FragmentListCompany = FragmentListCompany_.builder().build() }

    private val loginViewModel  by lazy { ViewModelProviders.of(activity!!).get(LoginViewModel::class.java) }

    private var disposable : Disposable? = null

    @AfterViews fun onInit() {

        loginViewModel.companyList.value?.let { onReceiveCompanyList(it) }

        observe(loginViewModel.companyList) { it?.let { onReceiveCompanyList(it)  } }

        companyListView.onSelect =  { kpiListPresenter.onSelectCompany(it) }

        save.setOnClickListener { kpiListPresenter.trySave() }
        disposable = kpiListPresenter.validateSelectionCompany
                ?.subscribe {
                    if (!it) toast(R.string.company_select)
                    else  { startActivity(Intent(activity, KpiActivity_::class.java)) + activity?.finish() }
                }
    }

    private fun onReceiveCompanyList(companies : List<Company>) {
        companyListView.setItems(companies.flatMap { listOf(CompanyItem(it)) })
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }
}