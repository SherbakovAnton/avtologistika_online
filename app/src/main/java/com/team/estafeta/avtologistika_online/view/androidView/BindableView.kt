package com.team.estafeta.avtologistika_online.view.androidView

/**
 * Created by antony on 10.04.18.
 */

interface BindableView<in E> {

    fun bind(e: E)
}