package com.team.estafeta.avtologistika_online.model.db.entity

import com.team.estafeta.avtologistika_online.model.db.realmCore.DataEntity
import io.realm.annotations.RealmClass

/**
 * Created by antony on 17.04.18.
 */
@RealmClass open class UserSettings : DataEntity {

    var isGotIt : Boolean = false

    override fun toString(): String = asString
}