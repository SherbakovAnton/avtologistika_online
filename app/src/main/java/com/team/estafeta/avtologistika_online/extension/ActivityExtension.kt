package com.team.estafeta.avtologistika_online.extension

import android.arch.lifecycle.*
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import kotlin.reflect.KClass

/**
 * Created by antony on 03.04.18.
 */

fun <T : ViewModel> FragmentActivity.obtainViewModel(viewModelClass: KClass<T>) =
        ViewModelProviders.of(this).get(viewModelClass.java)

fun <T : ViewModel> FragmentActivity.obtainViewModel(viewModelClass: KClass<T>, initFoo : () -> T) =
        ViewModelProviders.of(this).get(viewModelClass.java).apply { initFoo() }

fun <T> FragmentActivity.observe(liveData: LiveData<T>, callback: (T?) -> Unit) {
    liveData.observe(this, Observer { callback.invoke(it) })
}

fun FragmentActivity.lifeCycle(observer : LifecycleObserver) = this.lifecycle.addObserver(observer)

fun <T : FragmentActivity> FragmentActivity.openActivityAfterClosingCurrent(clazz : KClass<T>) {
    startActivity(Intent(this, clazz.java)) + finish()
}

/************************************ TOOLBAR  ***********************************/

fun AppCompatActivity.basicInitActionBar(toolbar : Toolbar) : Toolbar {
    setSupportActionBar(toolbar)
    supportActionBar?.title = null
    return toolbar
}

/************************************ FRAGMENT TRANSACTIONS **********************/

fun FragmentActivity.setFragment(root : Int, fragment: Fragment) {
    this.supportFragmentManager.beginTransaction()
            .add(root, fragment)
            .commit()
}

fun FragmentActivity.addFragment(root : Int, fragment: Fragment) {
    this.supportFragmentManager.beginTransaction()
            .replace(root, fragment)
            .addToBackStack(null)
            .commit()
}

fun FragmentActivity.replaceFragment(root : Int, fragment: Fragment) {
    this.supportFragmentManager.beginTransaction()
            .replace(root, fragment)
            .commit()
}