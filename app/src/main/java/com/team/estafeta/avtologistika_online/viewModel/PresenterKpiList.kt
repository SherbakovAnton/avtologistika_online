package com.team.estafeta.avtologistika_online.viewModel

import com.team.estafeta.avtologistika_online.dataprovider.KpiDataProvider
import com.team.estafeta.avtologistika_online.model.db.entity.Company
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import com.team.estafeta.avtologistika_online.model.db.entity.UserState
import com.team.estafeta.avtologistika_online.model.db.repository.UserRepository
import com.team.estafeta.avtologistika_online.model.db.repository.saveUser
import io.reactivex.subjects.BehaviorSubject
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean

/**
 * Created by antony on 06.04.18.
 */

@EBean(scope = EBean.Scope.Singleton) open class PresenterKpiList {

    @Bean lateinit var dataProvider : KpiDataProvider

    var selectedCompany : Company? = null

    var kpiList : BehaviorSubject<List<Kpi>>? = null
    get() {
        if (field == null)
            field = BehaviorSubject.create<List<Kpi>>()
        return field
    }

    var validateSelectionCompany: BehaviorSubject<Boolean>? = null
        get() {
            if (field == null)
                field = BehaviorSubject.create<Boolean>()
            return field
        }

    /******************************************* METHODS ******************************************/

    fun onSelectCompany(company: Company) {
        selectedCompany = company

        UserRepository.getUser {
            it?.companyId = company.id
            it?.setState(UserState.companySelected)
            it?.saveUser { getListKpiAfterCompanyIsSelected() }
        }
    }

    fun getListKpiAfterCompanyIsSelected() {
        kpiList?.let { dataProvider.getKpiList(it) }
    }

    fun trySave() {

        selectedCompany?.let { company ->
            UserRepository.getUser {
                it?.companyId = company.id
                it?.saveUser()
                validateSelectionCompany?.onNext(true)
            }
        }
                ?:let { validateSelectionCompany?.onNext(false) }
    }

    fun dispose() {
        kpiList = null
        validateSelectionCompany = null
    }

}