package com.team.estafeta.avtologistika_online.view.fragment

/**
 * Created by antony on 23.04.18.
 */
interface ProgressDialog {

    fun progressDialogShow()

    fun progressDialogHide()
}