package com.team.estafeta.avtologistika_online.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.team.estafeta.avtologistika_online.model.db.repository.BaseRealmRepository

/**
 * Created by antony on 18.04.18.
 */
class SettingsViewModel : ViewModel() {

    val logOutLiveData = MutableLiveData<Unit>()

    fun logout(context: Context) {
        BaseRealmRepository.onLogout(context) {
            logOutLiveData.postValue(null)
        }
    }

}