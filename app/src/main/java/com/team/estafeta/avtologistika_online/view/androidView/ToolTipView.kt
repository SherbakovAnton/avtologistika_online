package com.team.estafeta.avtologistika_online.view.androidView

import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.Context
import android.databinding.BindingAdapter
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.initialize
import com.team.estafeta.avtologistika_online.extension.setAnimationHandler
import com.team.estafeta.avtologistika_online.model.db.repository.UserRepository
import kotlinx.android.synthetic.main.fragment_tooltip.view.*


/**
 * Created by antony on 12.04.18.
 */

open class ToolTipView(context : Context?, attrs : AttributeSet?)
                                : RelativeLayout(context, attrs) {

    val fadeIn : Animation by lazy { AnimationUtils.loadAnimation(context, R.anim.fade_in) }
    val transactionUp : Animation by lazy { AnimationUtils.loadAnimation(context, R.anim.transaction_to_up) }
    val fadeOut : Animation by lazy { AnimationUtils.loadAnimation(context, R.anim.fade_out) }


    var onGotItButtonClick : (() -> Unit)? = null



    private fun gotItPush() {

        onGotItButtonClick?.invoke()

        startAnimationHeight()
        this.startAnimation(fadeOut)
        this.startAnimation(transactionUp.setAnimationHandler {
            onAnimationEnd { this@ToolTipView.visibility = View.GONE }
        })
    }

    private fun startAnimationHeight() {
        val height = height

        val slideAnimator = ValueAnimator
                .ofInt(height, 0)
                .setDuration(fadeOut.duration)
        slideAnimator.addUpdateListener { animation ->
            val value = animation.animatedValue as Int
            layoutParams.height = value
            requestLayout()
        }

        val set = AnimatorSet()
        set.play(slideAnimator)
        set.interpolator = AccelerateDecelerateInterpolator()
        set.start()
    }

    init {
        initialize(R.layout.fragment_tooltip)

        visibility = View.GONE

        toolTipContent.text = context?.getString(R.string.tooltip_content)
        //this.startAnimation(fadeIn)

        gotIt.setOnClickListener { gotItPush() }
    }

}

@BindingAdapter("stateGotIt") fun RelativeLayout.stateGotIt_(isGotIt : Boolean) {
    ( this as? ToolTipView)?.let {
        if (!isGotIt) {
            this.onGotItButtonClick = { UserRepository.setUserGotIt() }
        } else {
            this.visibility = View.GONE
        }
    }

}