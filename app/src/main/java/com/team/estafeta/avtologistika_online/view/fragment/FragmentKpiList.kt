package com.team.estafeta.avtologistika_online.view.fragment

import android.arch.lifecycle.ViewModelProviders
import android.view.View
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.*
import com.team.estafeta.avtologistika_online.lifecycle.FragmentKpiListLifeCycle
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import com.team.estafeta.avtologistika_online.model.db.repository.UserRepository
import com.team.estafeta.avtologistika_online.model.db.repository.filterKpiWithNullProperties
import com.team.estafeta.avtologistika_online.model.db.repository.filterSelectedKpiList
import com.team.estafeta.avtologistika_online.view.androidView.KpiHorizontalPanel.Companion.isViewValid
import com.team.estafeta.avtologistika_online.viewModel.KpiDragAndDropViewModel
import com.team.estafeta.avtologistika_online.viewModel.PresenterKpiList
import kotlinx.android.synthetic.main.fragment_kpi_list.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EFragment


/**
 * Created by antony on 10.04.18.
 */

@EFragment(R.layout.fragment_kpi_list) open class FragmentKpiList : BaseFragment() {

    @Bean lateinit var kpiListPresenter : PresenterKpiList

    val dragAndDropViewModel        by lazy { ViewModelProviders.of(this).get(KpiDragAndDropViewModel::class.java) }

    private val lifeCycle           by lazy { FragmentKpiListLifeCycle(this) }

    private var onKpiClick : (Kpi) -> Unit = { onKpiClick(it) }

    @AfterViews fun onInit() {

        progressState = true

        disableBackArrow() + showGear()
        lifeCycle(lifeCycle)
                //handleTooltip()

        kpiListRecyclerView.onItemClick = onKpiClick
        kpiHorizontalPanel.onItemClick = onKpiClick

        kpiHorizontalPanel.onDragKpiListener = {
            it.isViewValid { dragAndDropViewModel.onKpiDragged(it) }
        }
    }

    private fun handleTooltip() {
        //if (toolTipRoot.visibility == View.GONE) return
        UserRepository.isUserGotIt {
            if (it) toolTipRoot.visibility = View.GONE
            else {
                toolTipRoot.visibility = View.VISIBLE
                toolTipRoot.onGotItButtonClick = { UserRepository.setUserGotIt() }
            }
        }
    }

     open fun onReceiveKpiList(list : List<Kpi>) {

         progressState = false
         handleTooltip()

         list.filterSelectedKpiList {
             it?.let { kpiHorizontalPanel.bindKpiList(it) }
         }

         list.filterKpiWithNullProperties {
             it?.let { kpiListRecyclerView.setItems(it) }
         }
    }

    private fun onKpiClick(kpi : Kpi){
        addFragment(R.id.root, FragmentKpi.newInstance(kpi))
    }

    companion object { fun newInstance() : FragmentKpiList = FragmentKpiList_.builder().build() }
}