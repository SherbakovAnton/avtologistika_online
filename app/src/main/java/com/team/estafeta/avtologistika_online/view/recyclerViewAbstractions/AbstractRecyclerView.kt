package com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.initialize

/**
 * Created by antony on 10.04.18.
 */
abstract class AbstractProgressRecyclerView<Item, ItemView : View>(context: Context?, attrs: AttributeSet?)
    : AbstractListView<Item, ItemView>(context, attrs) {

    //val progress        by lazy { findViewById<ProgressBar>(R.id.abstractProgress) }

    val recyclerView    by lazy { findViewById<RecyclerView>(R.id.abstractRecyclerView)
                                .apply { layoutManager = LinearLayoutManager(context) }
                                }

    override fun setItems(items: List<Item>) {
        //progress.visibility = View.GONE
        super.setItems(items)
    }

    init {
        initialize(R.layout.wraper_view_company)
    }


}