package com.team.estafeta.avtologistika_online.view.activity

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.view.Window
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.*
import com.team.estafeta.avtologistika_online.lifecycle.KpiActivityLifeCycle
import com.team.estafeta.avtologistika_online.viewModel.PresenterKpiList
import com.team.estafeta.avtologistika_online.viewModel.SettingsViewModel
import org.androidannotations.annotations.*

/**
 * Created by antony on 06.04.18.
 */




@SuppressLint("Registered")
@EActivity(R.layout.activity_kpi)
@WindowFeature(Window.FEATURE_NO_TITLE)
open class KpiActivity : AppCompatActivity() {

    @Bean lateinit var kpiListPresenter : PresenterKpiList

    private val settingsViewModel   by lazy { obtainViewModel(SettingsViewModel::class) }

    @AfterViews fun onInit() {
        kpiListPresenter.getListKpiAfterCompanyIsSelected()

        observe(settingsViewModel.logOutLiveData) { openActivityAfterClosingCurrent(MainActivity_::class) }

        lifeCycle(KpiActivityLifeCycle(this))
    }

    @OptionsItem(android.R.id.home) fun onHomeClicked() = onBackPressed()

    fun onLogOut() {
        alert {
            message = R.string.logout_message
            onResult = { settingsViewModel.logout(this@KpiActivity) }
        }
    }

}