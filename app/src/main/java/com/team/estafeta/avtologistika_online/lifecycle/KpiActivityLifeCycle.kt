package com.team.estafeta.avtologistika_online.lifecycle

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.basicInitActionBar
import com.team.estafeta.avtologistika_online.extension.setFragment
import com.team.estafeta.avtologistika_online.view.activity.KpiActivity
import com.team.estafeta.avtologistika_online.view.fragment.FragmentKpiList
import kotlinx.android.synthetic.main.activity_kpi.*

/**
 * Created by antony on 12.04.18.
 */
class KpiActivityLifeCycle(private val activity : KpiActivity) : LifecycleObserver {

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE) fun onCreate() {
        activity.run {
            setFragment(R.id.root, FragmentKpiList.newInstance())

            basicInitActionBar(toolbar)
            toolbarSettings.setOnClickListener {
                onLogOut()
                //addFragment(R.id.root, FragmentSettings.newInstance())
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY) fun onDestroy() {
        activity.run { kpiListPresenter.dispose() }
    }
}