package com.team.estafeta.avtologistika_online.model.db

/**
 * Created by antony on 24.04.18.
 */
data class ErrorLoading(val error : String = "this realm entity not exist")