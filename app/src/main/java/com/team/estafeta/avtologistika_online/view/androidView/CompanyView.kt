package com.team.estafeta.avtologistika_online.view.androidView

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.TextView
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.initialize
import com.team.estafeta.avtologistika_online.model.db.entity.Company
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.AbstractProgressRecyclerView
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.RecyclerViewAdapterBase
import com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions.ViewWrapper
import org.apache.commons.lang3.StringUtils

/**
 * Created by antony on 05.04.18.
 */


/** VIEW */
class CompanyListRecyclerView(context: Context, attrs: AttributeSet?) :
        AbstractProgressRecyclerView<CompanyItem, CompanyCellView>(context, attrs) {

    override val adapter        by lazy { CompanyesAdapter(context).apply { initAdapter() } }

    var onSelect : ((Company) -> Unit )? = null
    set(value) {
        field = value
        adapter.onSelect = value
    }

    init { recyclerView.adapter = adapter }

}
/** CELL */
class CompanyCellView(context: Context) : RelativeLayout(context) {

    private val companyNameTextView by lazy { findViewById<TextView>(R.id.companyName) }
    internal val companyRadioButton by lazy { findViewById<RadioButton>(R.id.companyRadioButton) }
    internal val mark               by lazy { findViewById<LinearLayout>(R.id.mark) }

    internal val root               by lazy { findViewById<RelativeLayout>(R.id.companyViewRoot) }

    internal var state : State? = null
    set(value) {
        field = value
        when (value) {
            State.white -> {
                companyNameTextView.setTextColor(ContextCompat.getColor(context, R.color.black))
                mark.visibility = View.INVISIBLE
            }
            State.blue -> {
                companyNameTextView.setTextColor(ContextCompat.getColor(context, R.color.gray))
                mark.visibility = View.VISIBLE
            }
        }

    }

    enum class State {
        white, blue
    }

    internal var company : CompanyItem? = null

    internal fun bind(company: CompanyItem) {
        this.company = company
        companyNameTextView.text = company.name
    }

    init { initialize(R.layout.cell_view_company) }
}

/** ITEM */
data class CompanyItem(val company: Company) {

    val id = company.id
    val name = company.name
}

/** ADAPTER */
class CompanyesAdapter(private val context: Context) : RecyclerViewAdapterBase<CompanyItem, CompanyCellView>() {

    private var selectedCompanyId = StringUtils.EMPTY

    internal var onSelect : ((Company) -> Unit)? = null
        set(value) { field = value ; notifyDataSetChanged() }

    override fun onBindViewHolder(holder: ViewWrapper<CompanyCellView>?, position: Int) {
        items[position].also { companyItem ->

            val state = if (position % CompanyCellView.State.values().size > 0)
                CompanyCellView.State.blue
            else
                CompanyCellView.State.white

            holder?.view?.let { view ->
                initView(companyItem, view)
                view.state = state
            }
        }
    }

    private fun initView(item : CompanyItem, view : CompanyCellView) : CompanyCellView {
        view.bind(item)
        view.companyRadioButton?.isChecked = selectedCompanyId.equals(item.id, true)
        view.root?.setOnClickListener {

            selectedCompanyId = item.id!!
            notifyItemRangeChanged(0, items.size)

            onSelect?.invoke(item.company)
        }
        return view
    }


    override fun onCreateItemView(parent: ViewGroup, viewType: Int): CompanyCellView = CompanyCellView(context)

}