package com.team.estafeta.avtologistika_online.widget

import android.content.Context

/**
 * Created by antony on 07.05.18.
 */
object WidgetManager {

     fun logout(context: Context) {
        GreenWidget.logout(context)
        WidgetEstafeta.logout(context)
    }

    fun updateInfo(context: Context) {
        GreenWidget.updateWidgetInfo(context)
        WidgetEstafeta.updateWidgetInfo(context)
    }
}