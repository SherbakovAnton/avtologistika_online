package com.team.estafeta.avtologistika_online.delegate.appDelegate

import android.util.Log

/**
 * Created by antony on 03.04.18.
 */

internal val log: (String) -> Unit = { Log.d("WIDGET", it) }