package com.team.estafeta.avtologistika_online.extension

import com.team.estafeta.avtologistika_online.WidgetApp

/**
 * Created by antony on 19.04.18.
 */

fun Any.string(int: Int) : String {
    return WidgetApp.appContext().getString(int)
}