package com.team.estafeta.avtologistika_online.extension

import android.content.Context
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Toast

/**
 * Created by antony on 06.04.18.
 */

fun Context.toast (res : Int) {
    Toast.makeText(this, res, Toast.LENGTH_SHORT).show()
}

fun Context.toast (message : String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun View.toast (message : String) = context?.toast(message)

fun View.toast (res : Int) = context?.toast(res)

fun Fragment.toast (message : String) = activity?.toast(message)

fun Fragment.toast (res : Int) = activity?.toast(res)
