package com.team.estafeta.avtologistika_online.viewModel

import android.arch.lifecycle.ViewModel
import com.team.estafeta.avtologistika_online.model.db.ErrorLoading
import com.team.estafeta.avtologistika_online.model.db.entity.User
import com.team.estafeta.avtologistika_online.model.db.repository.UserRepository
import io.reactivex.subjects.BehaviorSubject
import java.io.Serializable

/**
 * Created by antony on 03.04.18.
 */

class UserViewModel : ViewModel(), Serializable {

    val dataBaseUser : BehaviorSubject<User> = BehaviorSubject.create<User>()
    val errorLoad : BehaviorSubject<ErrorLoading> = BehaviorSubject.create()

    init {
        UserRepository.getUser {
            it
                    ?.let { dataBaseUser.onNext(it) }
                    ?: let { errorLoad.onNext(ErrorLoading()) }
        }
    }

}