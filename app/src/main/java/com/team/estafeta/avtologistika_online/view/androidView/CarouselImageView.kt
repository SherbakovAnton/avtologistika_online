package com.team.estafeta.avtologistika_online.view.androidView

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import com.team.estafeta.avtologistika_online.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by antony on 06.04.18.
 */

class CarouselImageView (context : Context?, attrs : AttributeSet?) : ImageView(context, attrs) {

    private val imageList = listOf(R.drawable.photo_bg_signin, R.drawable.photo_bg_signin_2)

    private val disposable : Disposable

    private val period = 15L

    init {
        disposable = Observable.interval(period, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { it % imageList.size }
                .subscribe { changeImage(it.toInt()) }
    }

    private fun changeImage(iterator : Int) {
        setImageResource(imageList[iterator])
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        disposable.dispose()
    }

}