package com.team.estafeta.avtologistika_online.model.web

import com.team.estafeta.avtologistika_online.model.db.entity.Company
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import com.team.estafeta.avtologistika_online.model.entity.Token
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Created by antony on 04.04.18.
 */

interface Api {

    @FormUrlEncoded
    @POST("/api/widget/login")
    fun getToken(@Field("login") login : String, @Field("password") password : String) : Flowable<Token>

    @POST("/api/widget/getCompanies")
    fun getCompanies() : Flowable<List<Company>>

    @POST("/api/widget/GetKeyPerformanceIndicators")
    fun getListKpi() : Flowable<List<Kpi>>


    @POST("/api/widget/GetKeyPerformanceIndicatorValues")
    fun getKpiValues(@Body request: List<GetKpiRequest>) : Flowable<List<Kpi>>

    /******************************************* REQUESTS *********************************************/

    data class GetKpiRequest( val keyPerformanceIndicatorId : String,
                              val brandId : String = "b0000000-0000-0000-0000-000000000002",
                              val dateFilterMode : Int = DateFilterMode.CurrentDate.ordinal) {
        var fromDate : String? = null
        var toDate : String? = null

        companion object {
            fun buildRequestArray(list: List<Kpi>) : List<GetKpiRequest> {
                return list
                        .filter { it.id != null }
                        .flatMap { listOf( GetKpiRequest(it.id!!)) }
            }
        }


        enum class DateFilterMode {
            None ,
            CurrentDate ,
            Period ;
        }
    }


}