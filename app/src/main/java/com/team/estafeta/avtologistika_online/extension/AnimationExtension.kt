package com.team.estafeta.avtologistika_online.extension

import android.view.animation.Animation
import com.team.estafeta.avtologistika_online.view.animation._AnimationListener

/**
 * Created by antony on 12.04.18.
 */

inline fun Animation.setAnimationHandler(func: _AnimationListener.() -> Unit) : Animation {
    _AnimationListener().also { it.func(); setAnimationListener(it) }
    return this
}


