package com.team.estafeta.avtologistika_online.model.web

import android.arch.lifecycle.MutableLiveData
import com.team.estafeta.avtologistika_online.delegate.appDelegate.log
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by antony on 06.04.18.
 */

fun MutableLiveData<ErrorWeb>.handle(throwable: Throwable) {
    log(throwable.cause.toString())

    when (throwable.cause) {
        is UnknownHostException,
        is SocketTimeoutException -> this.postValue(ErrorWeb.SocketTimeoutException)
        else -> this.postValue(ErrorWeb.UnknownHostException)
    }

}
