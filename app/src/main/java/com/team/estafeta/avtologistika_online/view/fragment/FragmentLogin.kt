package com.team.estafeta.avtologistika_online.view.fragment

import android.arch.lifecycle.ViewModelProviders
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.databinding.FragmentLoginBinding
import com.team.estafeta.avtologistika_online.delegate.appDelegate.log
import com.team.estafeta.avtologistika_online.extension.lifeCycle
import com.team.estafeta.avtologistika_online.extension.observe
import com.team.estafeta.avtologistika_online.extension.toast
import com.team.estafeta.avtologistika_online.lifecycle.ProgressLifeCycle
import com.team.estafeta.avtologistika_online.viewModel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.BindingObject
import org.androidannotations.annotations.DataBound
import org.androidannotations.annotations.EFragment


/**
 * Created by antony on 03.04.18.
 */

@DataBound
@EFragment(R.layout.fragment_login) open class FragmentLogin : BaseFragment() {

    private val loginViewModel  by lazy { ViewModelProviders.of(activity!!).get(LoginViewModel::class.java) }

    @AfterViews fun onInit() {
        lifeCycle(ProgressLifeCycle(this))

        signInButton.setOnClickListener { onSignIn() }

        observe(loginViewModel.errorWeb) { progressDialogHide() }
        observe(loginViewModel.companyList) { if (it.orEmpty().isEmpty()) progressDialogHide() }

        loginInput.textObserver.subscribe { log("FragmentLogin login - $it.") }
        passwordInput.textObserver.subscribe { log("FragmentLogin login - $it") }
    }

    @BindingObject fun injectBinding(binding: FragmentLoginBinding) { binding.viewModel = loginViewModel }

    //TODO оптимизировать валидацию https://stackoverflow.com/questions/3904579/how-to-capitalize-the-first-letter-of-a-string-in-java
    fun onSignIn() {

        //loginViewModel.login("vladimir", "vladimir") + return

        if (!loginInput.isValid() and !passwordInput.isValid()) {
            toast(R.string.login_or_password_empty)
            return
        }

        if (!loginInput.isValid() ) {
            toast(R.string.login_empty)
            return
        }

        if (!passwordInput.isValid() )  {
            toast(R.string.password_empty)
            return
        }

        loginViewModel.login(loginInput.text!!, passwordInput.text!!)

        progressDialogShow()
    }

    companion object { fun newInstance() : FragmentLogin = FragmentLogin_.builder().build() }
}