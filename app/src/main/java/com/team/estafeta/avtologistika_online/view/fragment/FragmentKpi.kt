package com.team.estafeta.avtologistika_online.view.fragment

import android.support.v4.app.Fragment
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.databinding.FragmentKpiUpBinding
import com.team.estafeta.avtologistika_online.extension.hideGear
import com.team.estafeta.avtologistika_online.extension.plus
import com.team.estafeta.avtologistika_online.extension.setBackArrow
import com.team.estafeta.avtologistika_online.extension.updateConstraints
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import com.team.estafeta.avtologistika_online.view.androidView.KpiTrainView
import kotlinx.android.synthetic.main.fragment_kpi_up.*
import org.androidannotations.annotations.*

/**
 * Created by antony on 13.04.18.
 */

val down = R.layout.fragment_kpi_down
val up = R.layout.fragment_kpi_up

@DataBound
@EFragment(R.layout.fragment_kpi_up) open class FragmentKpi : Fragment() {

    @FragmentArg lateinit var kpi : Kpi

    @BindingObject fun injectBinding(binding: FragmentKpiUpBinding) { binding.kpi = this.kpi }

    @AfterViews fun onInit() {
        ::setBackArrow + ::hideGear

        kpiTrainView_2.onTrain = {
            kpiScreenRoot.updateConstraints(if (it == KpiTrainView.TrainState.down) down else up)
        }
    }

    companion object { fun newInstance(kpi: Kpi): FragmentKpi = FragmentKpi_.builder().kpi(kpi).build() }

}