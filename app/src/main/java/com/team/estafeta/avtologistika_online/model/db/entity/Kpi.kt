package com.team.estafeta.avtologistika_online.model.db.entity

import com.team.estafeta.avtologistika_online.extension.toHHmm
import com.team.estafeta.avtologistika_online.model.db.realmCore.DataEntity
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import org.apache.commons.lang3.StringUtils

/**
 * Created by antony on 06.04.18.
 */

@RealmClass open  class Kpi : DataEntity {

    @PrimaryKey
    var id : String? = null

    var name : String? = null

    var description : String? = null

    private var value : String? = null


    var properties : KpiProperties? = null
    private set

    private var measurementTypeId : Int? = MeasurementType.nil.ordinal

    fun resetProperties() = setProperties()

    fun setProperties(index: Int? = null) : Kpi {
        index?.let {
            properties = KpiProperties().apply {
                this.index = it
                setUiPosition(KpiProperties.KpiUiPosition.inWidget)
            }
        } ?:let {
            properties = null
        }
        return this
    }

    fun getMeasurementType() : MeasurementType {
        return MeasurementType.values().find { it.ordinal == measurementTypeId } ?: MeasurementType.nil
    }

    fun setMeasurementType(type : MeasurementType) {
        measurementTypeId = type.ordinal
    }

    fun getValue() : String {
        return when (getMeasurementType()) {
            MeasurementType.sec     -> { value?.toHHmm() ?: StringUtils.EMPTY }
            MeasurementType.km      -> { value?.toDoubleOrNull()?.toInt()?.toString() ?: StringUtils.EMPTY }
            MeasurementType.car     -> { value?.toDoubleOrNull()?.toInt()?.toString() ?: StringUtils.EMPTY }
            MeasurementType.truck   -> { value?.toDoubleOrNull()?.toInt()?.toString() ?: StringUtils.EMPTY }
            else -> value.toString()
        }
    }

    fun merge(kpiFromServer : Kpi?) : Kpi? {

        kpiFromServer?.id?.let { this.id = it }
        kpiFromServer?.name?.let { this.name = it }
        kpiFromServer?.value?.let { this.value = it }
        kpiFromServer?.measurementTypeId?.let { this.measurementTypeId = it }
        kpiFromServer?.description?.let { this.description = it }


        /*this.id = kpiFromServer?.id
        this.name = kpiFromServer?.name
        this.value = kpiFromServer?.value
        this.measurementTypeId = kpiFromServer?.measurementTypeId
        this.description = kpiFromServer?.description*/
        return this
    }

    enum class MeasurementType { nil, sec, km, car, truck }

    override fun toString() = asString
}
