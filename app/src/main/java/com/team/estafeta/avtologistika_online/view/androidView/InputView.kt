package com.team.estafeta.avtologistika_online.view.androidView

import android.content.Context
import android.text.InputType.TYPE_CLASS_TEXT
import android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.jakewharton.rxbinding2.widget.RxTextView
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.initialize
import kotlinx.android.synthetic.main.view_input.view.*
import java.util.concurrent.TimeUnit

/**
 * Created by antony on 03.04.18.
 */
val res = R.layout.view_input
open class InputView(context : Context?, attrs : AttributeSet?) : RelativeLayout(context, attrs) {

    val textObserver         by lazy { RxTextView.afterTextChangeEvents(input.input) }

    var srcLeftIcon : Int? = null
        set(value) {
            field = value
            value?.let { leftIcon.setImageResource(it) }
        }

    var srcInputType : Boolean? = null
        set(value) {
            field = value
            value?.let {  if (it) input.inputType = TYPE_CLASS_TEXT or TYPE_TEXT_VARIATION_PASSWORD }
        }


    var text : String? = null
    get() = input.text.toString()
    set(value) {
        field = value
        input.setText(value)
    }

    var hint : String? = null
    set(value) {
        field = value
        input.hint = value
    }

    fun isValid() : Boolean {
        return !input.text.isNullOrEmpty()
    }

    init {
        initialize(res)
        val ta = context?.obtainStyledAttributes(attrs, R.styleable.InputView, 0, 0)
        try {
            srcLeftIcon = ta?.getResourceId(R.styleable.InputView_srcLeftIcon, 0)
            srcInputType = ta?.getBoolean(R.styleable.InputView_srcInputType, false)
            hint = ta?.getString(R.styleable.InputView_hint)
        } finally {
            ta?.recycle()
        }

        textObserver
                .skipInitialValue()
                .map { it.toString() }
                .debounce(1, TimeUnit.SECONDS)
    }

}
