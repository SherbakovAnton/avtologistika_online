package com.team.estafeta.avtologistika_online.extension

import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.transition.ChangeBounds
import android.transition.TransitionManager
import android.view.animation.OvershootInterpolator

/**
 * Created by antony on 13.04.18.
 */


fun ConstraintLayout.updateConstraints(id: Int) {
    val newConstraintSet = ConstraintSet()
    newConstraintSet.clone(this.context, id)
    newConstraintSet.applyTo(this)
    val transition = ChangeBounds()
    transition.interpolator = OvershootInterpolator()
    TransitionManager.beginDelayedTransition(this, transition)
}