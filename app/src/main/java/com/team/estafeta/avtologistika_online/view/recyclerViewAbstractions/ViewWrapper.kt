package com.team.estafeta.avtologistika_online.view.recyclerViewAbstractions

import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by antony on 05.04.18.
 */

class ViewWrapper<out V : View>(itemView : V) : RecyclerView.ViewHolder(itemView) {
    val view: V = itemView
}