package com.team.estafeta.avtologistika_online.view.androidView

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.DragEvent
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.gson.Gson
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.delegate.appDelegate.log
import com.team.estafeta.avtologistika_online.extension.initialize
import com.team.estafeta.avtologistika_online.extension.measuringSystem
import com.team.estafeta.avtologistika_online.extension.string
import com.team.estafeta.avtologistika_online.extension.toast
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi

@SuppressLint("WrongViewCast")
/**
 * Created by antony on 10.04.18.
 */
class KpiHorizontalPanel(context : Context?, attrs : AttributeSet?) : RelativeLayout(context, attrs) {

    var onDragKpiListener : ((KpiView) -> Unit)? = null
    set(value) {
        field = value
        kpi0.onDragKpiListener = field
        kpi1.onDragKpiListener = field
        kpi2.onDragKpiListener = field
    }

    var onItemClick : ((Kpi) -> Unit)? = null
        set(value) {
            listOf<KpiView>(kpi0, kpi1, kpi2)
                    .forEach { view ->
                        view.setOnClickListener {
                            view.entityKpi
                                    ?.let { kpi -> value?.invoke(kpi) }
                                    ?:let { toast(string(R.string.tooltip_content)) }
                        }
                    }
        }

    val kpi0 by lazy { findViewById<KpiView>(R.id.kpi0).apply { tag = 0 } }
    val kpi1 by lazy { findViewById<KpiView>(R.id.kpi1).apply { tag = 1 } }
    val kpi2 by lazy { findViewById<KpiView>(R.id.kpi2).apply { tag = 2 } }

    fun bindKpiList(list : List<Kpi>) {

        list.forEachIndexed { index, kpi ->
            listOf<KpiView>(kpi0, kpi1, kpi2)
                    .find { it.tag == kpi.properties?.index }
                    ?.let { it.entityKpi = kpi }
        }

        visibility = View.VISIBLE

    }

    init {
        initialize(R.layout.view_kpi_panel)

        visibility = View.INVISIBLE
    }

    companion object {

        fun KpiView.isViewValid(block : (Pair<Kpi, Int>) -> Unit) {
            if (this.entityKpi != null && (this.tag as? Int) != null) {
                block(Pair(this.entityKpi!!, (this.tag as Int)))
            }
        }

    }

}

class KpiView(context : Context?, attrs : AttributeSet?) : RelativeLayout(context, attrs), BindableView<Kpi> {

    val na      by lazy { findViewById<TextView>(R.id.viewKpiNA) }
    val value   by lazy { findViewById<TextView>(R.id.viewKpiElementValue) }
    val measure by lazy { findViewById<TextView>(R.id.viewKpiElementMeasuring) }
    val name    by lazy { findViewById<TextView>(R.id.viewKpiElementName) }

    var entityKpi : Kpi? = null
        set(newValue) {
            field = newValue
            newValue?.let {

                name.text = it.name
                na.visibility = View.GONE
                value.text = it.getValue()
                measure.text = it.measuringSystem()
            }
        }

    var onDragKpiListener : ((KpiView) -> Unit)? = null

    var serialized : String? = null
        set(newValue) {
            field = newValue
            log("KpiView - serialized - $field")

            try {
                bind(Gson().fromJson<Kpi>(newValue, Kpi::class.java))
            } catch (e : Exception) {
                e.printStackTrace()
            }
        }


    override fun bind(e: Kpi) {
        entityKpi = e
        onDragKpiListener?.invoke(this)
    }

    init {
        initialize(R.layout.view_kpi_element)

        setOnDragListener { _, dragEvent: DragEvent ->
            if (dragEvent.action == DragEvent.ACTION_DROP)
                serialized = dragEvent.clipData.description.label.toString()
            true
        }
    }

}