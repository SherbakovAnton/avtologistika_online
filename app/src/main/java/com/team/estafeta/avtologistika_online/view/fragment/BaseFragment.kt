package com.team.estafeta.avtologistika_online.view.fragment

import android.graphics.PorterDuff
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import com.afollestad.materialdialogs.MaterialDialog
import com.team.estafeta.avtologistika_online.R


/**
 * Created by antony on 23.04.18.
 */

open class BaseFragment : Fragment(), ProgressDialog {

    private var progress : MaterialDialog? = null
    private var progressBar : ProgressBar? = null

    var progressState : Boolean? = null
        set(value) {

            fun showProgress() {
                if (progressBar == null)  progressBar = initProgressBar()
                progressBar?.visibility = View.VISIBLE
            }

            fun hideProgress() {
                progressBar?.let { it.visibility = View.GONE }
            }
            value?.let { field = it; if (it) showProgress() else hideProgress() }
        }

    override fun progressDialogShow() {
        progress = MaterialDialog.Builder(activity!!)
                .content(R.string.connecting)
                .progress(true, 0)
                .cancelable(false)
                .show()
    }

    override fun progressDialogHide() {
        progress?.let {
            if(it.isShowing) it.cancel()
        }
    }

    private fun initProgressBar() : ProgressBar {
        val pr = ProgressBar(activity)
        pr.indeterminateDrawable.setColorFilter(ContextCompat.getColor(activity!!, R.color.colorAccent), PorterDuff.Mode.SRC_IN)
        val container = RelativeLayout(activity)

        container.layoutParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        params.addRule(RelativeLayout.CENTER_HORIZONTAL)
        params.addRule(RelativeLayout.CENTER_VERTICAL)
        pr.layoutParams = params
        container.addView(pr)
        (this@BaseFragment.view as? ViewGroup)?.addView(container)
        return pr
    }

}