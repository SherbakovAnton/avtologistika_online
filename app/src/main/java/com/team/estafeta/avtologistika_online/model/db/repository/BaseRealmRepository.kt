package com.team.estafeta.avtologistika_online.model.db.repository

import android.content.Context
import com.team.estafeta.avtologistika_online.extension.plus
import com.team.estafeta.avtologistika_online.widget.WidgetManager
import com.team.estafeta.avtologistika_online.widget.WidgetScheduler
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.estafeta.core.tracking.realm.clearRealm


/**
 * Created by antony on 18.04.18.
 */
object BaseRealmRepository {

    fun onLogout(context: Context, onCleared : () -> Unit) {

        Observable.create(ObservableOnSubscribe<Boolean> { emitter ->
            clearRealm() + emitter.onNext(true)

        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( {
                    onCleared()
                    WidgetScheduler.cancelSchedule()
                    WidgetManager.logout(context)
                } )
    }

}