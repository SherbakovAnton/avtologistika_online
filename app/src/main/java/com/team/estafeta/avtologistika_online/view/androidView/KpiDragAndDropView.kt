package com.team.estafeta.avtologistika_online.view.androidView

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import android.widget.TextView
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.extension.initialize
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi


/**
 * Created by antony on 11.04.18.
 */

open class KpiDragAndDropView(context : Context?, attrs : AttributeSet?) :
        RelativeLayout(context, attrs),
        BindableView<Kpi> {

    private val fadeOut : Animation

    private val value by lazy { findViewById<TextView>(R.id.dragAndDropValueKpi).apply {
        gravity = Gravity.CENTER
    } }

    final override fun bind(e: Kpi) {
        value.text = e.getValue()
    }

    constructor(context : Context) : this(context, null)
    constructor(context : Context, e: Kpi) : this(context) {
        bind(e)
    }

    override fun onDraw(canvas: Canvas?) {

        startAnimation(fadeOut)

    }


    init {
        initialize(R.layout.view_kpi_drag_and_drop)
        //gravity = Gravity.CENTER
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        val size = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        measure(size, size)
        layout(0, 0, this.measuredWidth, this.measuredHeight)

        fadeOut = AnimationUtils.loadAnimation(context, R.anim.abc_fade_out)
                //R.anim.bottom_up) context?.resources?.getAnimation(R.anim.fade_in)?.

        invalidate()
    }
}