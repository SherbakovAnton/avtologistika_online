package com.team.estafeta.avtologistika_online.widget

import android.app.IntentService
import android.content.Intent
import com.team.estafeta.avtologistika_online.dataprovider.KpiDataProvider
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EIntentService
import org.androidannotations.annotations.ServiceAction


@EIntentService open class WidgetIntentService : IntentService("WidgetIntentService") {

    @Bean
    @ServiceAction fun updateAllKpi(dataProvider : KpiDataProvider) {
        dataProvider.updateAllKpi()
    }

    override fun onHandleIntent(p0: Intent?) {}
}
