package com.team.estafeta.avtologistika_online.model.db.entity

import com.team.estafeta.avtologistika_online.model.db.realmCore.DataEntity
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by antony on 04.04.18.
 */



@RealmClass open class Company : DataEntity {

    @PrimaryKey
    var id : String? = null

    var name : String? = null

    override fun toString() = asString
}