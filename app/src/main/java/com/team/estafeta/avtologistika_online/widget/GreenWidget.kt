package com.team.estafeta.avtologistika_online.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.RemoteViews
import com.team.estafeta.avtologistika_online.R
import com.team.estafeta.avtologistika_online.delegate.appDelegate.log
import com.team.estafeta.avtologistika_online.extension.measuringSystem
import com.team.estafeta.avtologistika_online.extension.string
import com.team.estafeta.avtologistika_online.model.db.entity.Kpi
import com.team.estafeta.avtologistika_online.model.db.entity.KpiHistory
import com.team.estafeta.avtologistika_online.model.db.entity.toDate
import com.team.estafeta.avtologistika_online.model.db.repository.UserRepository
import com.team.estafeta.avtologistika_online.model.db.repository.getSelectedKpiList
import org.apache.commons.lang3.StringUtils
import org.estafeta.core.tracking.realm.getFirstEntity

/**
 * Created by antony on 03.05.18.
 */
open class GreenWidget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {

            updateAppWidget(context, appWidgetManager, appWidgetId)
            checkUser(context)
            //updateWidgetInfo(context)
        }


    }

    override fun onEnabled(context: Context) {

        initViews(context).apply {
            val pendingIntent = PendingIntent.getActivity(context, 0, Intent(context, StartActivity_::class.java), 0)
            setOnClickPendingIntent(R.id.greenWidgetRoot, pendingIntent)
            updateWidget(context, this)
            checkUser(context)
        }

       // updateWidgetInfo(context)

    }

    override fun onDisabled(context: Context) {
        WidgetScheduler.cancelSchedule()
    }

    companion object {

        fun checkUser(context: Context) {
            val views = initViews(context)
            UserRepository.getUser {
                it?.let { getSelectedKpi(context, views) }
                        ?: let { showNotLogin(context, views) }
            }
        }

        fun showNotLogin(context: Context, remoteViews: RemoteViews) {
            showMessage(context, remoteViews, context.string(R.string.auth_text_widget))
        }

        private fun showMessage(context : Context, remoteViews : RemoteViews, message : String) {
            remoteViews.setViewVisibility(R.id.widgetKpiContainer, View.INVISIBLE)
            remoteViews.setViewVisibility(R.id.noSelectedKpi, View.VISIBLE)
            remoteViews.setTextViewText(R.id.noSelectedKpi, message)
            updateWidget(context, remoteViews)
        }

        fun getSelectedKpi(context: Context, remoteViews: RemoteViews) {
            getSelectedKpiList({ list ->
                list
                        .also { if (list == null) showNoKpiSelected(context, remoteViews) }
                        ?.forEach {
                            WidgetScheduler.schedule(context)
                            updateWidget(context, remoteViews, it) }
            })
        }

        fun logout(context: Context) {
            val views = initViews(context)

            showNoKpiSelected(context, views)

            val na = string(R.string.na)

            views.setTextViewText(R.id.widget_kpi0_name, na)
            views.setTextViewText(R.id.widget_kpi0_value, na)
            views.setTextViewText(R.id.widget_kpi0_si, StringUtils.EMPTY)

            views.setTextViewText(R.id.widget_kpi1_name, na)
            views.setTextViewText(R.id.widget_kpi1_value, na)
            views.setTextViewText(R.id.widget_kpi1_si, StringUtils.EMPTY)

            views.setTextViewText(R.id.widget_kpi2_name, na)
            views.setTextViewText(R.id.widget_kpi2_value, na)
            views.setTextViewText(R.id.widget_kpi2_si, StringUtils.EMPTY)


            checkUser(context)
            updateWidget(context, views)

        }

        fun updateWidgetInfo(context: Context) {
            val views = initViews(context)

            getSelectedKpiList({ list ->
                list
                        .also { if (list == null) showNoKpiSelected(context, views) }
                        ?.forEach {
                            WidgetScheduler.schedule(context)
                            updateWidget(context, views, it) }
            })
        }

        private fun showNoKpiSelected(context: Context, remoteViews : RemoteViews) {
            showMessage(context, remoteViews, context.string(R.string.kpi_not_selected))
        }

        private fun updateWidget(context: Context, remoteViews : RemoteViews, kpi : Kpi) {

            remoteViews.setViewVisibility(R.id.noSelectedKpi, View.INVISIBLE)

            when (kpi.properties?.index) {
                0 -> {
                    remoteViews.setTextViewText(R.id.widget_kpi0_name, kpi.name)
                    remoteViews.setTextViewText(R.id.widget_kpi0_value, kpi.getValue())
                    remoteViews.setTextViewText(R.id.widget_kpi0_si, kpi.measuringSystem())
                }
                1 -> {
                    remoteViews.setTextViewText(R.id.widget_kpi1_name, kpi.name)
                    remoteViews.setTextViewText(R.id.widget_kpi1_value, kpi.getValue())
                    remoteViews.setTextViewText(R.id.widget_kpi1_si, kpi.measuringSystem())
                }
                2 -> {
                    remoteViews.setTextViewText(R.id.widget_kpi2_name, kpi.name)
                    remoteViews.setTextViewText(R.id.widget_kpi2_value, kpi.getValue())
                    remoteViews.setTextViewText(R.id.widget_kpi2_si, kpi.measuringSystem())
                }
            }
            remoteViews.setViewVisibility(R.id.widgetKpiContainer, View.VISIBLE)

            remoteViews.setTextViewText(R.id.widgetDate, "${context.getString(R.string.last_update)} ${KpiHistory::class.getFirstEntity()?.toDate() ?: "12 September 15:45"}" )

            updateWidget(context, remoteViews)
        }


        internal fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager,
                                     appWidgetId: Int) {

            log("updateAppWidget")

            val widgetText = context.getString(R.string.appwidget_text)
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.widget_green)
            //views.setTextViewText(R.id.appwidget_text, widgetText)

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }

        open fun initViews(context: Context) = RemoteViews(context.packageName, R.layout.widget_green)


        open fun updateWidget(context: Context, remoteViews: RemoteViews)
                = AppWidgetManager.getInstance(context).updateAppWidget(ComponentName(context, GreenWidget::class.java), remoteViews)

    }
}